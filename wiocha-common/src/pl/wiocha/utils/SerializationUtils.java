package pl.wiocha.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationUtils {
	public static byte[] serialize(Object obj) {
		ByteArrayOutputStream ms = new ByteArrayOutputStream();
		ObjectOutputStream out;
		
		try {
			out = new ObjectOutputStream(ms);
			out.writeObject(obj);
			out.close();
			return ms.toByteArray();
		}
		catch (IOException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T deserialize(byte[] bytes) {
		ByteArrayInputStream is = new ByteArrayInputStream(bytes);
		ObjectInputStream ois;
		
		T obj = null;
		try {
			ois = new ObjectInputStream(is);
			obj = (T)ois.readObject();
			ois.close();
		}
		catch (Exception e) {
		}
		
		return obj;
	}
}
