package pl.wiocha.network.messages;

import java.io.Serializable;

/**
 * Message being sent when player logged in successfully
 */
public class LoginResultMessage implements Serializable {
	private static final long serialVersionUID = 2137575898776718539L;
	
	public String playerId;
	
	public LoginResultMessage() {
	}
}
