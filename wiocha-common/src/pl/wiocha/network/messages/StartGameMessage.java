package pl.wiocha.network.messages;

import java.io.Serializable;

import pl.wiocha.enums.PlayerType;

public class StartGameMessage implements Serializable {
	private static final long serialVersionUID = -8413903605847255663L;
	
	public String attackerPlayerId;
	public String defenderPlayerId;
	public String mapId;
	
	public PlayerType getPlayerType(String playerId) {
		boolean isAttacker = attackerPlayerId.equals(playerId);
		return isAttacker ? PlayerType.Attack : PlayerType.Defense;
	}
}
