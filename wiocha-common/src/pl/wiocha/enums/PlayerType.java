package pl.wiocha.enums;

/**
 * Player type also defines player id.
 * 
 * @author Namek
 */
public enum PlayerType {
	None, Defense, Attack;
	
	private static PlayerType[] _values;
	
	static {
		_values = PlayerType.values();
	}

	public static PlayerType getOpposite(PlayerType type) {
		return type == Defense ? Attack : Defense;
	}

	public PlayerType getOpposite() {
		return ordinal() == Defense.ordinal() ? Attack : Defense;
	}

	public boolean isOpposite(PlayerType playerType)  {
		assert (this != None);
		return playerType != this;
	}

	public static PlayerType fromInt(int value) {
		return _values[value];
    }
	
	public int toInt() {
		return ordinal();
	}

	public static PlayerType getRandom() {
		return fromInt((int)(System.currentTimeMillis() % 2) + 1);
	}
}
