package pl.wiocha.listeners;

import pl.wiocha.actors.FieldActor;
import pl.wiocha.actors.UnitActor;

public interface BoardListener {
	void unitTapped(UnitActor unitActor);
	void fieldTapped(FieldActor unitActor);
}
