package pl.wiocha.input;

import pl.wiocha.enums.Constants;
import pl.wiocha.utils.SpeedController;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class KeyboardTestInputProcessor extends InputAdapter {
	private int horizontalDirection = 0;
	private int verticalDirection = 0;
	private OrthographicCamera _camera;
	private final SpeedController _speedController;


	public KeyboardTestInputProcessor(OrthographicCamera camera, SpeedController speedController) {
		_camera = camera;
		_speedController = speedController;
	}


	public boolean keyDown(int keycode) {
		boolean keyHandled = true;

		if (keycode == Keys.PAGE_UP) {
			_camera.zoom -= 0.05;
		}
		else if (keycode == Keys.PAGE_DOWN) {
			_camera.zoom += 0.05;
		}
		else if (updateCameraSpeed()) {
			
		}
		else {
			keyHandled = false;
		}

		return keyHandled;
	}

	public boolean keyUp(int keycode) {
		boolean keyHandled = true;

		if (updateCameraSpeed()) {
			
		}
		else {
			keyHandled = false;
		}

		return keyHandled;
	}

	private boolean updateCameraSpeed() {
		final Input input = Gdx.input;
		final int previousHorzDir = horizontalDirection;
		final int previousVertDir = verticalDirection;

		if (input.isKeyPressed(Keys.RIGHT)) {
			horizontalDirection = 1;
		}
		else if (input.isKeyPressed(Keys.LEFT)) {
			horizontalDirection = -1;
		}
		else {
			horizontalDirection = 0;
		}

		if (input.isKeyPressed(Keys.UP)) {
			verticalDirection = 1;
		}
		else if (input.isKeyPressed(Keys.DOWN)) {
			verticalDirection = -1;
		}
		else {
			verticalDirection = 0;
		}

		boolean keyHandled = horizontalDirection != previousHorzDir || verticalDirection != previousVertDir;
		
		if (keyHandled) {
			_speedController.acceleration.x = Constants.BoardCamera.Acceleration * horizontalDirection;
			_speedController.acceleration.y = Constants.BoardCamera.Acceleration * verticalDirection;
			_speedController.isFrictionXOn = horizontalDirection == 0;
			_speedController.isFrictionYOn = verticalDirection == 0;
			
			if (horizontalDirection != 0 && previousHorzDir == 0) {
				_speedController.velocity.x = 0;
			}
			
			if (verticalDirection != 0 && previousVertDir == 0) {
				_speedController.velocity.y = 0;
			}
		}
		
		return keyHandled;
	}
}
