package pl.wiocha.utils;

import static aurelienribon.tweenengine.TweenEquations.easeNone;

import java.util.Stack;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquation;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.TweenPaths;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class TimelineBuilder {
	private static final TweenManager _tweenManager = new TweenManager();
	
	static {
		Tween.registerAccessor(Actor.class, new ActorAccessor());
		Tween.setCombinedAttributesLimit(4);
		Tween.setWaypointsLimit(10);
	}
	
	public static void update(float deltaTime) {
		_tweenManager.update(deltaTime);
		Tween.setCombinedAttributesLimit(4);
	}
	
	private Timeline _rootTimeline, _currentTimeline;
	private Stack<Timeline> _timelinesHierarchy = new Stack<Timeline>();
	private Actor _currentActor;
	
	
	public static TimelineBuilder createSequence() {
		TimelineBuilder builder = new TimelineBuilder();
		builder.beginSequence();
		
		return builder;
	}

	public TimelineBuilder beginSequence() {
		if (_rootTimeline == null) {
			_rootTimeline = Timeline.createSequence();
			_currentTimeline = _rootTimeline;
		}
		else {
			Timeline previousTimeline = _currentTimeline;
			_currentTimeline = Timeline.createSequence();
			previousTimeline.push(_currentTimeline);
			
			_timelinesHierarchy.add(previousTimeline);
		}
		
		return this;
	}
	
	public TimelineBuilder beginParallel() {
		if (_rootTimeline == null) {
			_rootTimeline = Timeline.createParallel();
			_currentTimeline = _rootTimeline;
		}
		else {
			Timeline previousTimeline = _currentTimeline;
			_currentTimeline = Timeline.createParallel();
			previousTimeline.push(_currentTimeline);
			
			_timelinesHierarchy.add(previousTimeline);
		}
		
		return this;
	}
	
	public TimelineBuilder end() {
		_currentTimeline = _timelinesHierarchy.pop();
		
		return this;
	}
	
	public void start() {
		while (_currentTimeline != _rootTimeline) {
			end();
		}
		
		_rootTimeline.start(_tweenManager);
	}
	
	
	// Basic actions
	
	public TimelineBuilder wait(float duration) {
		_currentTimeline.pushPause(duration);
		return this;
	}
	
	public TimelineBuilder call(TweenCallback callback) {
		_currentTimeline.push(Tween.call(callback));
		return this;
	}
	
	public TimelineBuilder setActor(Actor actor) {
		_currentActor = actor;
		return this;
	}
	
	public TimelineBuilder repeat(int count, float delay) {
		_currentTimeline.repeat(count, delay);
		return this;
	}
	
	public TimelineBuilder repeatYoyo(int count, float delay) {
		_currentTimeline.repeatYoyo(count, delay);
		return this;
	}
	
	
	// Waypoints
	
	public WaypointsBuilder moveThroughCatmulRomWaypoints(float duration, float ... xys) {
		return moveThroughWaypoints(true, duration, xys);
	}
	
	public WaypointsBuilder moveThroughCatmulRomWaypoints(float duration, TweenEquation ease, float ... xys) {
		return moveThroughWaypoints(true, duration, ease, xys);
	}
	
	public WaypointsBuilder moveThroughCatmulRomWaypoints(Actor actor, float duration, float ... xys) {
		return moveThroughWaypoints(actor, true, duration, xys);
	}
	
	public WaypointsBuilder moveThroughCatmulRomWaypoints(Actor actor, float duration, TweenEquation ease, float ... xys) {
		return moveThroughWaypoints(true, duration, ease, xys);
	}
	
	public WaypointsBuilder moveThroughLinearWaypoints(float duration, float ... xys) {
		return moveThroughWaypoints(false, duration, xys);
	}
	
	public WaypointsBuilder moveThroughLinearWaypoints(float duration, TweenEquation ease, float ... xys) {
		return moveThroughWaypoints(false, duration, ease, xys);
	}
	
	public WaypointsBuilder moveThroughLinearWaypoints(Actor actor, float duration, float ... xys) {
		return moveThroughWaypoints(actor, false, duration, xys);
	}
	
	public WaypointsBuilder moveThroughLinearWaypoints(Actor actor, float duration, TweenEquation ease, float ... xys) {
		return moveThroughWaypoints(false, duration, ease, xys);
	}
	
	public WaypointsBuilder moveThroughWaypoints(boolean catmulRom, float duration, float ... xys) {
		return moveThroughWaypoints(catmulRom, duration, easeNone, xys);
	}

	public WaypointsBuilder moveThroughWaypoints(boolean catmulRom, float duration, TweenEquation ease, float ... xys) {
		return moveThroughWaypoints(_currentActor, catmulRom, duration, ease, xys);
	}
	
	public WaypointsBuilder moveThroughWaypoints(Actor actor, boolean catmulRom, float duration, float ... xys) {
		return moveThroughWaypoints(actor, catmulRom, duration, easeNone, xys);
	}
	
	public WaypointsBuilder moveThroughWaypoints(Actor actor, boolean catmulRom, float duration, TweenEquation ease, float ... xys) {
		assert(xys.length % 2 == 0);
		
		WaypointsBuilder waypointsBuilder = beginWaypoints(actor, catmulRom, duration, ease);
		for (int i = 0, n = xys.length; i < n; i += 2) {
			waypointsBuilder.waypoint(xys[i], xys[i+1]);
		}
		return waypointsBuilder;
	}
	
	public TimelineBuilder.WaypointsBuilder beginCatmulRomWaypoints(float duration) {
		return beginWaypoints(_currentActor, true, duration, easeNone);
	}
	
	public TimelineBuilder.WaypointsBuilder beginCatmulRomWaypoints(float duration, TweenEquation ease) {
		return beginWaypoints(_currentActor, true, duration, ease);
	}

	public TimelineBuilder.WaypointsBuilder beginCatmulRomWaypoints(Actor actor, float duration) {
		return beginCatmulRomWaypoints(actor, duration, easeNone);
	}

	public TimelineBuilder.WaypointsBuilder beginCatmulRomWaypoints(Actor actor, float duration, TweenEquation ease) {
		return beginWaypoints(actor, true, duration, ease);
	}
	
	public TimelineBuilder.WaypointsBuilder beginLinearWaypoints(float duration) {
		return beginLinearWaypoints(_currentActor, duration);
	}
	
	public TimelineBuilder.WaypointsBuilder beginLinearWaypoints(float duration, TweenEquation ease) {
		return beginLinearWaypoints(_currentActor, duration, ease);
	}
	
	public TimelineBuilder.WaypointsBuilder beginLinearWaypoints(Actor actor, float duration) {
		return beginLinearWaypoints(actor, duration, easeNone);
	}

	public TimelineBuilder.WaypointsBuilder beginLinearWaypoints(Actor actor, float duration, TweenEquation ease) {
		return beginWaypoints(actor, false, duration, ease);
	}
	
	public TimelineBuilder.WaypointsBuilder beginWaypoints(boolean catmulRom, float duration) {
		return beginWaypoints(_currentActor, catmulRom, duration, easeNone);
	}
	
	public TimelineBuilder.WaypointsBuilder beginWaypoints(boolean catmulRom, float duration, TweenEquation ease) {
		return beginWaypoints(_currentActor, catmulRom, duration, ease);
	}
	
	public TimelineBuilder.WaypointsBuilder beginWaypoints(Actor actor, boolean catmulRom, float duration, TweenEquation ease) {
		return new WaypointsBuilder(this, actor, catmulRom, duration, ease);
	}
	
	public class WaypointsBuilder {
		private TimelineBuilder _timelineBuilder;
		private Tween _producedTween;
		
		WaypointsBuilder(TimelineBuilder timelineBuilder, Actor actor, boolean catmulRom, float duration, TweenEquation ease) {
			_timelineBuilder = timelineBuilder;
			_producedTween = Tween.to(actor, ActorAccessor.XY, duration).ease(ease).path(catmulRom ? TweenPaths.catmullRom : TweenPaths.linear);
		}
		
		public WaypointsBuilder waypoint(float x, float y) {
			_producedTween.waypoint(x, y);
			return this;
		}
		
		public TimelineBuilder target(float x, float y) {
			_timelineBuilder._currentTimeline.push(_producedTween.target(x, y));
			return _timelineBuilder;
		}
		
		public TimelineBuilder targetRelative(float x, float y) {
			_timelineBuilder._currentTimeline.push(_producedTween.targetRelative(x, y));
			return _timelineBuilder;
		}
	}
	
	
	// Move To
	
	public TimelineBuilder moveTo(Actor actor, float x, float y, float duration) {
		return moveTo(actor, x, y, duration, easeNone);
	}
	
	public TimelineBuilder moveTo(float x, float y, float duration) {
		return moveTo(_currentActor, x, y, duration);
	}
	
	public TimelineBuilder moveTo(float x, float y, float duration, TweenEquation ease) {
		return moveTo(_currentActor, x, y, duration, ease);
	}
	
	public TimelineBuilder moveTo(Actor actor, float x, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.XY, duration).target(x, y).ease(ease));
		return this;
	}
	
	public TimelineBuilder moveToX(Actor actor, float x, float duration) {
		return moveToX(actor, x, duration, easeNone);
	}
	
	public TimelineBuilder moveToX(float x, float duration) {
		return moveToX(_currentActor, x, duration);
	}
	
	public TimelineBuilder moveToX(float x, float duration, TweenEquation ease) {
		return moveToX(_currentActor, x, duration, ease);
	}
	
	public TimelineBuilder moveToX(Actor actor, float x, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.X, duration).target(x).ease(ease));
		return this;
	}
	
	public TimelineBuilder moveToY(Actor actor, float y, float duration) {
		return moveToY(actor, y, duration, easeNone);
	}
	
	public TimelineBuilder moveToY(float y, float duration) {
		return moveToY(_currentActor, y, duration);
	}
	
	public TimelineBuilder moveToY(float y, float duration, TweenEquation ease) {
		return moveToY(_currentActor, y, duration, ease);
	}
	
	public TimelineBuilder moveToY(Actor actor, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.Y, duration).target(y).ease(ease));
		return this;
	}
	
	
	// Move By
	
	public TimelineBuilder moveBy(Actor actor, float x, float y, float duration) {
		return moveBy(actor, x, y, duration, easeNone);
	}
	
	public TimelineBuilder moveBy(float x, float y, float duration) {
		return moveBy(_currentActor, x, y, duration);
	}
	
	public TimelineBuilder moveBy(float x, float y, float duration, TweenEquation ease) {
		return moveBy(_currentActor, x, y, duration, ease);
	}
	
	public TimelineBuilder moveBy(Actor actor, float x, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.XY, duration).targetRelative(x, y).ease(ease));
		return this;
	}
	
	public TimelineBuilder moveByX(Actor actor, float x, float duration) {
		return moveByX(actor, x, duration, easeNone);
	}
	
	public TimelineBuilder moveByX(float x, float duration) {
		return moveByX(_currentActor, x, duration);
	}
	
	public TimelineBuilder moveByX(float x, float duration, TweenEquation ease) {
		return moveByX(_currentActor, x, duration, ease);
	}
	
	public TimelineBuilder moveByX(Actor actor, float x, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.X, duration).targetRelative(x).ease(ease));
		return this;
	}
	
	public TimelineBuilder moveByY(Actor actor, float y, float duration) {
		return moveByY(actor, y, duration, easeNone);
	}
	
	public TimelineBuilder moveByY(float y, float duration) {
		return moveByY(_currentActor, y, duration);
	}
	
	public TimelineBuilder moveByY(float y, float duration, TweenEquation ease) {
		return moveByY(_currentActor, y, duration, ease);
	}
	
	public TimelineBuilder moveByY(Actor actor, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.Y, duration).targetRelative(y).ease(ease));
		return this;
	}
	
	
	// Scale To
	
	public TimelineBuilder scaleTo(Actor actor, float scale, float duration) {
		return scaleTo(actor, scale, duration, easeNone);
	}
	
	public TimelineBuilder scaleTo(float scale, float duration) {
		return scaleTo(_currentActor, scale, duration);
	}
	
	public TimelineBuilder scaleTo(float scale, float duration, TweenEquation ease) {
		return scaleTo(_currentActor, scale, duration, ease);
	}
	
	public TimelineBuilder scaleTo(Actor actor, float scale, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.SCALE, duration).target(scale, scale).ease(ease));
		return this;
	}
	
	public TimelineBuilder scaleToXY(Actor actor, float x, float y, float duration) {
		return scaleToXY(actor, x, y, duration, easeNone);
	}
	
	public TimelineBuilder scaleToXY(float x, float y, float duration) {
		return scaleToXY(_currentActor, x, y, duration);
	}
	
	public TimelineBuilder scaleToXY(float x, float y, float duration, TweenEquation ease) {
		return scaleToXY(_currentActor, x, y, duration, ease);
	}
	
	public TimelineBuilder scaleToXY(Actor actor, float x, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.SCALE, duration).target(x, y).ease(ease));
		return this;
	}
	
	public TimelineBuilder scaleToX(Actor actor, float x, float duration) {
		return scaleToX(actor, x, duration, easeNone);
	}
	
	public TimelineBuilder scaleToX(float x, float duration) {
		return scaleToX(_currentActor, x, duration);
	}
	
	public TimelineBuilder scaleToX(float x, float duration, TweenEquation ease) {
		return scaleToX(_currentActor, x, duration, ease);
	}
	
	public TimelineBuilder scaleToX(Actor actor, float x, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.SCALE_X, duration).target(x).ease(ease));
		return this;
	}
	
	public TimelineBuilder scaleToY(Actor actor, float y, float duration) {
		return scaleToY(actor, y, duration, easeNone);
	}
	
	public TimelineBuilder scaleToY(float y, float duration) {
		return scaleToY(_currentActor, y, duration);
	}
	
	public TimelineBuilder scaleToY(float y, float duration, TweenEquation ease) {
		return scaleToY(_currentActor, y, duration, ease);
	}
	
	public TimelineBuilder scaleToY(Actor actor, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.Y, duration).target(y).ease(ease));
		return this;
	}
	
	
	// Scale By
	
	public TimelineBuilder scaleBy(Actor actor, float x, float y, float duration) {
		return scaleBy(actor, x, y, duration, easeNone);
	}
	
	public TimelineBuilder scaleBy(float x, float y, float duration) {
		return scaleBy(_currentActor, x, y, duration);
	}
	
	public TimelineBuilder scaleBy(float x, float y, float duration, TweenEquation ease) {
		return scaleBy(_currentActor, x, y, duration, ease);
	}
	
	public TimelineBuilder scaleBy(Actor actor, float x, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.SCALE, duration).targetRelative(x, y).ease(ease));
		return this;
	}
	
	public TimelineBuilder scaleByX(Actor actor, float x, float duration) {
		return scaleByX(actor, x, duration, easeNone);
	}
	
	public TimelineBuilder scaleByX(float x, float duration) {
		return scaleByX(_currentActor, x, duration);
	}
	
	public TimelineBuilder scaleByX(float x, float duration, TweenEquation ease) {
		return scaleByX(_currentActor, x, duration, ease);
	}
	
	public TimelineBuilder scaleByX(Actor actor, float x, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.SCALE_X, duration).targetRelative(x).ease(ease));
		return this;
	}
	
	public TimelineBuilder scaleByY(Actor actor, float y, float duration) {
		return scaleByY(actor, y, duration, easeNone);
	}
	
	public TimelineBuilder scaleByY(float y, float duration) {
		return scaleByY(_currentActor, y, duration);
	}
	
	public TimelineBuilder scaleByY(float y, float duration, TweenEquation ease) {
		return scaleByY(_currentActor, y, duration, ease);
	}
	
	public TimelineBuilder scaleByY(Actor actor, float y, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.SCALE_Y, duration).targetRelative(y).ease(ease));
		return this;
	}
	
	
	// Rotate To
	
	public TimelineBuilder rotateTo(Actor actor, float y, float duration) {
		return rotateTo(actor, y, duration, easeNone);
	}
	
	public TimelineBuilder rotateTo(float angle, float duration) {
		return rotateTo(_currentActor, angle, duration);
	}
	
	public TimelineBuilder rotateTo(float angle, float duration, TweenEquation ease) {
		return rotateTo(_currentActor, angle, duration, ease);
	}
	
	public TimelineBuilder rotateTo(Actor actor, float angle, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.ROTATION, duration).target(angle).ease(ease));
		return this;
	}

	
	// Rotate By
	
	public TimelineBuilder rotateBy(Actor actor, float angle, float duration) {
		return rotateBy(actor, angle, duration, easeNone);
	}
	
	public TimelineBuilder rotateBy(float angle, float duration) {
		return rotateBy(_currentActor, angle, duration);
	}
	
	public TimelineBuilder rotateBy(float angle, float duration, TweenEquation ease) {
		return rotateBy(_currentActor, angle, duration, ease);
	}
	
	public TimelineBuilder rotateBy(Actor actor, float angle, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.ROTATION, duration).targetRelative(angle).ease(ease));
		return this;
	}
	
	
	// Color To
	
	public TimelineBuilder colorTo(Actor actor, Color color, float duration) {
		return colorTo(actor, color, duration, easeNone);
	}
	
	public TimelineBuilder colorTo(Color color, float duration) {
		return colorTo(_currentActor, color, duration);
	}
	
	public TimelineBuilder colorTo(Color color, float duration, TweenEquation ease) {
		return colorTo(_currentActor, color, duration, ease);
	}
	
	public TimelineBuilder colorTo(Actor actor, Color color, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.RGBA, duration).target(color.r, color.g, color.b, color.a).ease(ease));
		return this;
	}
	

	public TimelineBuilder colorTo(Actor actor, float r, float g, float b, float duration) {
		return colorTo(actor, r, g, b, duration, easeNone);
	}
	
	public TimelineBuilder colorTo(float r, float g, float b, float duration) {
		return colorTo(_currentActor, r, g, b, duration, easeNone);
	}
	
	public TimelineBuilder colorTo(float r, float g, float b, float duration, TweenEquation ease) {
		return colorTo(_currentActor, r, g, b, duration, ease);
	}
	
	public TimelineBuilder colorTo(Actor actor, float r, float g, float b, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.RGB, duration).target(r, g, b).ease(ease));
		return this;
	}
	
	
	// Alpha To
	
	public TimelineBuilder alphaTo(Actor actor, float alpha, float duration) {
		return alphaTo(actor, alpha, duration, easeNone);
	}
	
	public TimelineBuilder alphaTo(float alpha, float duration) {
		return alphaTo(_currentActor, alpha, duration, easeNone);
	}
	
	public TimelineBuilder alphaTo(float alpha, float duration, TweenEquation ease) {
		return alphaTo(_currentActor, alpha, duration, ease);
	}
	
	public TimelineBuilder alphaTo(Actor actor, float alpha, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.ALPHA, duration).target(alpha).ease(ease));
		return this;
	}
	
	
	
	// Fade In
	
	public TimelineBuilder fadeIn(Actor actor, float duration) {
		return fadeIn(actor, duration, easeNone);
	}
	
	public TimelineBuilder fadeIn(float duration) {
		return fadeIn(_currentActor, duration, easeNone);
	}
	
	public TimelineBuilder fadeIn(Actor actor, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.ALPHA, duration).target(1f).ease(ease));
		return this;
	}
	
	
	// Fade Out

	public TimelineBuilder fadeOut(Actor actor, float duration) {
		return fadeOut(actor, duration, easeNone);
	}
	
	public TimelineBuilder fadeOut(float duration) {
		return fadeOut(_currentActor, duration, easeNone);
	}
	
	public TimelineBuilder fadeOut(Actor actor, float duration, TweenEquation ease) {
		_currentTimeline.push(Tween.to(actor, ActorAccessor.ALPHA, duration).target(0f).ease(ease));
		return this;
	}
	
	
	
	
	private static class ActorAccessor implements TweenAccessor<Actor> {
		static final int X = 1;
		static final int Y = 2;
		static final int XY = 3;
		static final int SCALE_X = 4;
		static final int SCALE_Y = 5;
		static final int SCALE = 6;
		static final int ROTATION = 7;
		static final int ALPHA = 8;
		static final int RGB = 9;
		static final int RGBA = 10;

		@Override
		public int getValues(Actor target, int tweenType, float[] returnValues) {
			Color col;

			switch (tweenType) {
				case X: returnValues[0] = target.getX(); return 1;
				case Y: returnValues[0] = target.getY(); return 1;
				case XY:
					returnValues[0] = target.getX();
					returnValues[1] = target.getY();
					return 2;
				case SCALE_X: returnValues[0] = target.getScaleX(); return 1;
				case SCALE_Y: returnValues[0] = target.getScaleY(); return 1;
				case SCALE:
					returnValues[0] = target.getScaleX();
					returnValues[1] = target.getScaleY();
					return 2;
				case ROTATION: returnValues[0] = target.getRotation(); return 1;
				case ALPHA: returnValues[0] = target.getColor().a; return 1;
				case RGB:
					col = target.getColor();
					returnValues[0] = col.r;
					returnValues[1] = col.g;
					returnValues[2] = col.b;
					return 3;
				case RGBA:
					col = target.getColor();
					returnValues[0] = col.r;
					returnValues[1] = col.g;
					returnValues[2] = col.b;
					returnValues[3] = col.a;
					return 4;
				default: assert false; return 0;
			}
		}

		@Override
		public void setValues(Actor target, int tweenType, float[] newValues) {
			switch (tweenType) {
				case X: target.setX(newValues[0]); break;
				case Y: target.setY(newValues[0]); break;
				case XY: target.setPosition(newValues[0], newValues[1]); break;
				case SCALE_X: target.setScaleX(newValues[0]); break;
				case SCALE_Y: target.setScaleY(newValues[0]); break;
				case SCALE: target.setScale(newValues[0], newValues[1]); break;
				case ROTATION: target.setRotation(newValues[0]); break;
				case ALPHA: target.getColor().a = newValues[0]; break;
				case RGB:
					Color color = target.getColor();
					color.set(newValues[0], newValues[1], newValues[2], color.a);
					break;
				case RGBA:
					target.getColor().set(newValues[0], newValues[1], newValues[2], newValues[3]);
					break;
				default: assert false;
			}
		}
		
	}
}
