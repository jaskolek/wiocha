package pl.wiocha.utils.callbacks;

public interface ResultCallback<ResultType> {
	void onCallback(ResultType result);
}
