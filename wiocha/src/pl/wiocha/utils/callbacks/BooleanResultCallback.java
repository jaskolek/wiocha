package pl.wiocha.utils.callbacks;

public interface BooleanResultCallback extends ResultCallback<Boolean> {
}
