package pl.wiocha.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * position in axial coordinate system. encapsulates methods like getting length
 * between 2 positions etc
 * 
 * @author jaskolek
 * 
 */
public class AxialPosition {
	public int col;
	public int row;

	public AxialPosition() {
		this(0, 0);
	}

	public AxialPosition(int row, int col) {
		super();
		this.col = col;
		this.row = row;
	}

	public AxialPosition cpy() {
		return new AxialPosition(row, col);
	}

	/**
	 * returns all positions in distance N
	 * result doesn't contains center position
	 * @param N
	 * @return
	 */
	public Array<AxialPosition> getPositionsInDistance(int N) {
		Array<AxialPosition> positions = new Array<AxialPosition>();

		for (int dr = -N; dr <= N; ++dr) {
			int minDc = Math.max(-N, -N - dr);
			int maxDc = Math.min(N, N - dr);

			for (int dc = minDc; dc <= maxDc; ++dc) {
				if (!(dr == 0 && dc == 0)) {
					positions.add(new AxialPosition(row + dr, col + dc));
				}
			}
		}

		return positions;
	}

	public Array<AxialPosition> getNeighbors() {
		return getPositionsInDistance(1);
		// Array<AxialPosition> neighbors = new Array<AxialPosition>(false, 6);
		//
		// neighbors.add(new AxialPosition(row - 1, col));
		// neighbors.add(new AxialPosition(row - 1, col + 1));
		// neighbors.add(new AxialPosition(row, col + 1));
		// neighbors.add(new AxialPosition(row + 1, col));
		// neighbors.add(new AxialPosition(row + 1, col - 1));
		// neighbors.add(new AxialPosition(row, col - 1));
		//
		// return neighbors;
	}


	/**
	 * Returns normalized center of position in coordinates.
	 * 1 = length of hex side
	 * 
	 * @return
	 */
	public Vector2 getNormalizedCenterCoordinates() {
		Vector2 coordinates = new Vector2();

		coordinates.x = (float) Math.sqrt(3) * ((col + 1) + row * 0.5f - 0.5f);
		coordinates.y = 1.5f * (row + 1) - 0.5f;

		return coordinates;
	}

	/**
	 * Gets rotation to targetPosition
	 * 
	 * @param targetPosition
	 * @return
	 */
	public float getRotationToPosition(AxialPosition targetPosition) {
		Vector2 diffVector = targetPosition.getNormalizedCenterCoordinates().sub(getNormalizedCenterCoordinates());

		return diffVector.angle();
	}

	public float getRotationToPosition(int targetRow, int targetCol) {
		return getRotationToPosition(new AxialPosition(targetRow, targetCol));
	}

	public float getDistance(AxialPosition position) {
		return (Math.abs(row - position.row) + Math.abs(position.col - col)
		+ Math.abs(row + col - position.row - position.col)) * 0.5f;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AxialPosition other = (AxialPosition) obj;
		if (this.row == other.row && this.col == other.col) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + row;
		result = prime * result + col;
		return result;
	}
}
