package pl.wiocha.utils;

import com.badlogic.gdx.math.Vector2;

public class SpeedController {
	private final Vector2 tmpVelocity = new Vector2();
	
	public final Vector2 velocity = new Vector2();
	public final Vector2 acceleration = new Vector2();
	public float maxSpeed;
	public float friction;
	public boolean isFrictionXOn;
	public boolean isFrictionYOn;

	public final Vector2 positionDelta = new Vector2();


	public SpeedController() {
	}

	public SpeedController(float maxSpeed) {
		this.maxSpeed = maxSpeed;
		this.friction = 0;
	}

	public SpeedController(float maxSpeed, float friction) {
		this.maxSpeed = maxSpeed;
		this.friction = friction;
	}

	public float getCurrentSpeed() {
		return velocity.len();
	}

	public void update(float deltaTime) {
		// Calculate velocity
		tmpVelocity.set(acceleration).scl(deltaTime).add(velocity).limit(maxSpeed);

		// Apply optional friction
		if (isFrictionXOn || isFrictionYOn) {
			float friction = this.friction * deltaTime;;
			float speedX = Math.abs(tmpVelocity.x);
			float speedY = Math.abs(tmpVelocity.y);
			
			if (isFrictionXOn) {
				if (friction < speedX) {
					tmpVelocity.x *= 1f / speedX * -friction;
				}
				else {
					tmpVelocity.x = velocity.x * -1f;
				}
				
				velocity.x += tmpVelocity.x;
			}
			else {
				velocity.x = tmpVelocity.x;
			}
			
			if (isFrictionYOn) {
				if (friction < speedY) {
					tmpVelocity.y *= 1f / speedY * -friction;
				}
				else {
					tmpVelocity.y = velocity.y * -1f;
				}
				
				velocity.y += tmpVelocity.y;
			}
			else {
				velocity.y = tmpVelocity.y;
			}
		}
		else {
			velocity.set(tmpVelocity);
		}
		
		// Calculate position delta
		positionDelta.set(velocity).scl(deltaTime);
	}
}
