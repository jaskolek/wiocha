package pl.wiocha.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

/**
 * Wrapper for libgdx logging. Logs a debug message to the console or logcat.
 * 
 * Useful usage:
 * <code>import static pl.wiocha.utils.Logger.*;
 * log("bla bla");
 * error("omg");
 * </code>
 * 
 * @author nmk
 */
public abstract class Logger {
	public static final String TAG = "Vioha";
	
	{
		setLogLevel(Application.LOG_DEBUG);
	}
	
	public static void log(String message) {
		Gdx.app.log(TAG, message);
	}
	
	public static void log(String message, Object ... args) {
		StringBuilder argsStr = new StringBuilder(message);
		
		for (int i = 0; i < args.length; ++i) {
			argsStr.append(args[i].toString());
			if (i < args.length - 1) {
				argsStr.append(", ");
			}
		}
		
		Gdx.app.log(TAG, argsStr.toString());
	}
	
	public static void error(String message) {
		Gdx.app.error(TAG, message);
	}
	
	public static void error(String message, Throwable exception) {
		Gdx.app.error(TAG, message, exception);
	}
	
	public static void debug (String message) {
		Gdx.app.debug(TAG, message);
	}

	public static void debug (String message, Throwable exception) {
		Gdx.app.debug(TAG, message, exception);
	}

	/** Sets the log level. {@link #LOG_NONE} will mute all log output. {@link #LOG_ERROR} will only let error messages through.
	 * {@link #LOG_INFO} will let all non-debug messages through, and {@link #LOG_DEBUG} will let all messages through.
	 * @param logLevel {@link #LOG_NONE}, {@link #LOG_ERROR}, {@link #LOG_INFO}, {@link #LOG_DEBUG}. */
	public static void setLogLevel (int logLevel) {
		Gdx.app.setLogLevel(logLevel);
	}

	/** Gets the log level. */
	public static int getLogLevel() {
		return Gdx.app.getLogLevel();
	}
}
