package pl.wiocha.utils;

import pl.wiocha.utils.callbacks.VoidCallback;

import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public abstract class GlobalTime {	
	public static float timeScaleFactor = 1f;
	public static float deltaTime;
	
	private static WaitTask _waitTask = new WaitTask();
	
	
	public static float getDeltaTime() {
		return getDeltaTime(false);
	}
	
	public static float getDeltaTime(boolean scaled) {
		return scaled ? deltaTime * timeScaleFactor : deltaTime;
	}
	
	public static float getScaledDeltaTime() {
		return getDeltaTime(true);
	}

	/**
	 * Waits given amount of time and calls callback.
	 * Warning: Does not take time scaling into account!
	 */
	public static void waitAndDoOnce(float delaySeconds, VoidCallback callback) {
		_waitTask.callback = callback;
		Timer.schedule(_waitTask, delaySeconds);
	}
	
	
	private static class WaitTask extends Task {
		VoidCallback callback;
		
		@Override
		public void run() {
			callback.onCallBack();
		}
	}
}
