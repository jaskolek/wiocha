package pl.wiocha.utils;

import static com.badlogic.gdx.math.Interpolation.exp5Out;
import static com.badlogic.gdx.math.Interpolation.swingOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class ToastBox extends Actor {
	public static ToastBox instance;
	private BitmapFont _font;
	private NinePatch _ninePatch;
	private TextBounds _bounds = new TextBounds();
	private String _text = "asdsadsadas";
	
	private static final float HIDE_DURATION = 0.5f;
	private static final float SHOW_DURATION = 0.3f;
	
	
	public ToastBox(Skin skin) {
		_font = new BitmapFont();
		_ninePatch = skin.getPatch("default-round-large");
		_ninePatch.getColor().a = 0.8f;
		setVisible(false);
		
		resetPosition();
	}
	
	public static void show(String text, float time) {
		instance.clearActions();
		instance.resetPosition();
		instance._show(text, time);
	}
	
	public static void show(String text) {
		instance.clearActions();
		instance.resetPosition();
		instance._show(text);
	}
	
	public static void hide() {
		instance.clearActions();
		instance.resetPosition();
		instance._hide();
	}
	
	public static void hideInstant() {
		instance._hide(0);
	}
	
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!isVisible()) {
			return;
		}
		
		_font.getBounds(_text, _bounds);
		
		float textX = getX() - _bounds.width / 2;
		float textY = getY();
		
		final float MARGIN = 10;
		float bgWidth = _bounds.width + MARGIN*2;
		float bgHeight = _bounds.height + MARGIN*2;
		float bgX = textX - MARGIN;
		float bgY = textY - _bounds.height - MARGIN;
		
		float alpha = getColor().a;
		_ninePatch.getColor().a = MathUtils.clamp(alpha, 0, 0.8f);
		
		_ninePatch.draw(batch, bgX, bgY, bgWidth, bgHeight);
		_font.draw(batch, _text, textX, textY);
	}

	@Override
	public void setScale(float scale) {
		super.setScale(scale);
		_font.setScale(scale);
	}

	@Override
	public void setScale(float scaleX, float scaleY) {
		super.setScale(scaleX, scaleY);
		_font.setScale(scaleX, scaleY);
	}

	public void setText(String text) {
		_text = text;
	}
	
	private void resetPosition() {
		setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()*0.1f);
	}
	
	/**
	 * Shows text that will disappear after given amount of time.
	 * 
	 * @param text
	 * @param duration
	 */
	private void _show(String text, float duration) {
		setText(text);
		animateShow();
		
		if (duration > 0) {
			addAction(delay(duration, run(new Runnable() {
				@Override
				public void run() {
					_hide();
				}
			})));
		}
	}
	
	/**
	 * Shows text that will disappear after some calculated time/
	 * 
	 * @param text
	 */
	private void _show(String text) {
		float duration = text.length() * 0.12f;
		_show(text, duration);
	}
	
	private void animateShow() {
		clearActions();
		resetPosition();
		setScale(0.2f);
		getColor().a = 1f;
		setVisible(true);
		addAction(scaleTo(1f, 1f, 0.3f, swingOut));
	}
	
	private void _hide(float hideDuration) {
		addAction(
			sequence(
				parallel(
					scaleTo(0.2f, 0.2f, hideDuration, exp5Out),
					moveBy(0, -100, hideDuration, exp5Out),
					alpha(0.2f, hideDuration, exp5Out)
				),
				run(new Runnable() {
					@Override
					public void run() {
						setVisible(false);
					}
				})
			)
		);
	}
	
	private void _hide() {
		_hide(HIDE_DURATION);
	}
}
