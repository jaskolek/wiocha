package pl.wiocha.utils;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class Content {
	private final Map<String, Table> _tables = new HashMap<String, Table>();
	private final Array<String> _tableNames;
	
	
	public Content(String jsonAsText) {
		JsonValue root = new JsonReader().parse(jsonAsText);
		
		_tableNames = new Array<String>();
		
		for (int i = 0; i < root.size; ++i) {
			JsonValue jsonTable = root.get(i);
			Table table = new Table(jsonTable);
			
			_tableNames.add(table.name);
			_tables.put(table.name, table);
		}
	}
	
	public Content(FileHandle file) {
		this(file.readString());
	}
	
	public Array<String> getTableNames() {
		return _tableNames;
	}

	public int getColumnIndex(String tableName, String wantedColumnName) {
		return getTable(tableName).getColumnIndex(wantedColumnName);
	}

	public String getValueByRefColumn(String tableName, String refColumnName, Object refValue, String wantedColumnName) {
		return getTable(tableName).getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asString();
	}

	public int getValueByRefColumnAsInt(String tableName, String refColumnName, Object refValue, String wantedColumnName) {
		return getTable(tableName).getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asInt();
	}
	
	public float getValueByRefColumnAsFloat(String tableName, String refColumnName, Object refValue, String wantedColumnName) {
		return getTable(tableName).getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asFloat();
	}
	
	public boolean getValueByRefColumnAsBoolean(String tableName, String refColumnName, Object refValue, String wantedColumnName) {
		return getTable(tableName).getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asBoolean();
	}

	public String getValue(String tableName, Object idValue, String wantedColumnName) {
		return getValueByRefColumn(tableName, "id", idValue, wantedColumnName);
	}
	
	public int getValueAsInt(String tableName, Object idValue, String wantedColumnName) {
		return getValueByRefColumnAsInt(tableName, "id", idValue, wantedColumnName);
	}

	public float getValueAsFloat(String tableName, Object idValue, String wantedColumnName) {
		return getValueByRefColumnAsFloat(tableName, "id", idValue, wantedColumnName);
	}
	
	public boolean getValueAsBoolean(String tableName, Object idValue, String wantedColumnName) {
		return getValueByRefColumnAsBoolean(tableName, "id", idValue, wantedColumnName);
	}
	
	public String getParameterValue(String id) {
		return getValue("parameter", id, "value");
	}
	
	public int getParameterAsInt(String id) {
		return getValueAsInt("parameter", id, "value");
	}
	
	/*
	public <T> Parameters getRowAsParametersByRefColumn(String tableName, String refColumnName, T refValue) {
		throw new UnsupportedOperationException("to be implemented");
	}
	
	public <T> Parameters getRowAsParameters(String tableName, T idValue) {
		return getRowAsParametersByRefColumn(tableName, "id", idValue);
	}
	*/
	
	public Table getTable(String tableName) {
		return _tables.get(tableName);
	}
	
	public Table.Row getFirstRowByRefColumn(String tableName, String refColumnName, Object refValue) {
		return getTable(tableName).getFirstRowByRefColumn(refColumnName, refValue);
	}
	
	public Array<Table.Row> getRowsByRefColumn(String tableName, String refColumnName, Object refValue) {
		return getRowsByRefColumn(tableName, refColumnName, refValue, Integer.MAX_VALUE, null);
	}
	
	public Array<Table.Row> getRowsByRefColumn(String tableName, String refColumnName, Object refValue, int maxAmount) {
		return getRowsByRefColumn(tableName, refColumnName, refValue, Integer.MAX_VALUE, null);
	}
	
	public Array<Table.Row> getRowsByRefColumn(String tableName, String refColumnName, Object refValue, Array<Table.Row> outputRows) {
		return getRowsByRefColumn(tableName, refColumnName, refValue, Integer.MAX_VALUE, outputRows);
	}
	
	public Array<Table.Row> getRowsByRefColumn(String tableName, String refColumnName, Object refValue, int maxAmount, Array<Table.Row> outputRows) {
		return getTable(tableName).getRowsByRefColumn(refColumnName, refValue, maxAmount, outputRows);
	}
	
	
	
	public static class Table {
		public final String name;
		public final Array<String> columnNames = new Array<String>(false, 16, String.class); 
		public final Array<Row> rows = new Array<Row>(false, 100, Row.class);
		private final Array<Table.Row> _tmpRowsArray;
		
		
		public Table(JsonValue table) {
			this.name = table.name;
			
			JsonValue columns = table.get(0);
			for (int i = 0; i < columns.size; ++i) {
				String name = columns.getString(i).toString();
				columnNames.add(name);
			}
			
			for (int i = 1; i < table.size; ++i) {
				Row row = new Row(table.get(i));
				rows.add(row);
			}
			
			_tmpRowsArray = new Array<Row>(false, rows.size, Row.class);
		}
		
		public String getName() {
			return name;
		}
		
		public int getColumnIndex(String wantedColumnName) {
			for (int i = 0; i < columnNames.size; ++i) {
				String columnName = columnNames.get(i);
				if (columnName.equals(wantedColumnName)) {
					return i;
				}
			}
			
			return -1;
		}
		
		public String getColumnName(int index) {
			return columnNames.items[index];
		}
		
		public String getValueByRefColumn(String refColumnName, Object refValue, String wantedColumnName) {
			return getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asString();
		}

		public int getValueByRefColumnAsInt(String refColumnName, String refValue, String wantedColumnName) {
			return getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asInt();
		}
		
		public boolean getValueByRefColumnAsBoolean(String refColumnName, String refValue, String wantedColumnName) {
			return getJsonValueByRefColumn(refColumnName, refValue, wantedColumnName).asBoolean();
		}

		public String getValue(Object idValue, String wantedColumnName) {
			return getValueByRefColumn("id", idValue, wantedColumnName);
		}
		
		public Row getFirstRowByRefColumn(String refColumnName, Object refValue) {
			_tmpRowsArray.clear();
			getRowsByRefColumn(refColumnName, refValue, 1, _tmpRowsArray);
			
			return _tmpRowsArray.size > 0 ? _tmpRowsArray.first() : null;
		}
		
		public Array<Row> getRowsByRefColumn(String refColumnName, Object refValue) {
			return getRowsByRefColumn(refColumnName, refValue, Integer.MAX_VALUE, null);
		}
		
		public Array<Row> getRowsByRefColumn(String refColumnName, Object refValue, int maxAmount) {
			return getRowsByRefColumn(refColumnName, refValue, Integer.MAX_VALUE, null);
		}
		
		public Array<Row> getRowsByRefColumn(String refColumnName, Object refValue, Array<Row> outputRows) {
			return getRowsByRefColumn(refColumnName, refValue, Integer.MAX_VALUE, outputRows);
		}
		
		public Array<Row> getRowsByRefColumn(String refColumnName, Object refValue, int maxAmount, Array<Row> outputRows) {
			int refColumnIndex = getColumnIndex(refColumnName);
			
			int newArraySize = maxAmount > 0 && maxAmount < Integer.MAX_VALUE ? maxAmount : 16; 
			Array<Row> foundRows = outputRows != null ? outputRows : new Array<Row>(false, newArraySize, Row.class);
			int foundRowsCount = 0;
			
			for (Row row : this.rows) {
				if (row.cells.get(refColumnIndex).equals(refValue) || row.cells.get(refColumnIndex).asString().equals(refValue.toString())) {
					foundRows.add(row);
					foundRowsCount++;
					
					if (foundRowsCount >= maxAmount) {
						break;
					}
				}
			}
			
			return foundRows;
		}
		
		private JsonValue getJsonValueByRefColumn(String refColumnName, Object refValue, String wantedColumnName) {
			int wantedColumnIndex = getColumnIndex(wantedColumnName);
			Table.Row row = getFirstRowByRefColumn(refColumnName, refValue);
			assert(wantedColumnIndex >= 0);
			
			return row.cells.get(wantedColumnIndex);
		}
		
		
		
		public class Row {
			private final Array<JsonValue> cells = new Array<JsonValue>();
			
			public Row(JsonValue row) {
				for (int i = 0; i < row.size; ++i) {
					cells.add(row.get(i));
				}
				assert(cells.size == columnNames.size);
			}
			
			public Array<JsonValue> getCells() {
				return cells;
			}
			
			public String getString(int columnIndex) {
				return cells.get(columnIndex).asString();
			}
			
			public String getString(String columnName) {
				final int columnIndex = getColumnIndex(columnName);
				return getString(columnIndex);
			}
			
			public int getInt(int columnIndex) {
				return cells.get(columnIndex).asInt();
			}
			
			public int getInt(String columnName) {
				final int columnIndex = getColumnIndex(columnName);
				return getInt(columnIndex);
			}
			
			public boolean getBoolean(int columnIndex) {
				return cells.get(columnIndex).asBoolean();
			}
			
			public boolean getBoolean(String columnName) {
				final int columnIndex = getColumnIndex(columnName);
				return getBoolean(columnIndex);
			}
			
			public String[] getStringArray(int columnIndex) {
				String str = cells.get(columnIndex).asString();
				String[] values;

				int beginIndex = str.indexOf('[');
				int endIndex = str.length()-1;
				if (beginIndex < 0) {
					beginIndex = 0;
				}
				else {
					endIndex = str.indexOf(']') - 1;
				}
				
				String withoutBrackets = str.substring(beginIndex, endIndex + 1);
				if (withoutBrackets.length() == 0) {
					values = new String[0];
				}
				else {
					values = withoutBrackets.split(",");
					for (int i = 0; i < values.length; ++i) {
						values[i] = values[i].trim();
					}
				}
				return values;
			}
			
			public String[] getStringArray(String columnName) {
				final int columnIndex = getColumnIndex(columnName);
				return getStringArray(columnIndex);
			}
			
			public int[] getIntArray(int columnIndex) {
				String[] strValues = getStringArray(columnIndex);
				int n = strValues.length;
				int[] intValues = new int[n];
				
				for (int i = 0; i < n; ++i) {
					intValues[i] = Integer.valueOf(strValues[i]);
				}
				
				return intValues;
			}
			
			public int[] getIntArray(String columnName) {
				final int columnIndex = getColumnIndex(columnName);
				return getIntArray(columnIndex);
			}

			public <T extends Enum<T>> T getEnum(String columnName, Class<T> enumClass) {
				final int columnIndex = getColumnIndex(columnName);
				return getEnum(columnIndex, enumClass);
			}
			
			public <T extends Enum<T>> T getEnum(int columnIndex, Class<T> enumClass) {
				String value = getString(columnIndex);
				
				T retEnumVal = null;
				try {
					String valueFirstLetterBig = Character.toUpperCase(value.charAt(0)) + value.substring(1);
					retEnumVal = Enum.valueOf(enumClass, valueFirstLetterBig);
				}
				catch (Exception ex) {
					try {
						retEnumVal = Enum.valueOf(enumClass, value);
					}
					catch (Exception ex2) { }
				}
				
				return retEnumVal;
			}
		}
	}
}