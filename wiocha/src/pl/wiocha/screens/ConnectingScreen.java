package pl.wiocha.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

import pl.wiocha.DataContext;
import pl.wiocha.enums.PlayerType;
import pl.wiocha.logic.PlayerState;
import pl.wiocha.network.listeners.IConnectionListener;
import pl.wiocha.network.listeners.IStartGameSessionListener;
import pl.wiocha.network.messages.StartGameMessage;
import pl.wiocha.utils.Logger;
import pl.wiocha.utils.ToastBox;

public class ConnectingScreen extends BaseScreen {
	private DataContext _dataContext;
	
	public ConnectingScreen(Game game, DataContext dataContext) {
		super(game, dataContext.batch);
		_dataContext = dataContext;
		
		final String username = "nmk" + System.currentTimeMillis();
		final String password = "";
		final PlayerType requestedPlayerType = PlayerType.None;
		
		Gdx.graphics.setTitle(username + "  playerType = " + requestedPlayerType);
		
		_dataContext.backend.initialize();
		_dataContext.backend.connect(username, password, new IConnectionListener() {
			@Override
			public void onConnectionResult(boolean successfullyConnected) {
				if (successfullyConnected) {
					_dataContext.backend.createOrJoinRoom(requestedPlayerType, new IStartGameSessionListener() {
						@Override
						public void onNewGameSessionStatus(int statusCode, Object obj) {
							if (statusCode == GAME_STARTED) {
								StartGameMessage gameInfo = (StartGameMessage)obj;
								gotoGameScreen(gameInfo);
								ToastBox.show("Starting the game already!");
							}
							else {
								Logger.error("omg couldn't connect etc.");
								ToastBox.show("omg couldn't connect etc.!");
							}
						}
					});
				}
			}
		});
	}

	@Override
	public void render(float deltaTime) {
		_basicStage.act(deltaTime);
		_basicStage.draw();
	}
	
	private void gotoGameScreen(StartGameMessage gameInfo) {
		final PlayerState thisPlayerState = new PlayerState(gameInfo.getPlayerType(_dataContext.backend.getPlayerId()));
		_dataContext.thisPlayerId = thisPlayerState.getPlayerType();
		_dataContext.currentPlayerId = _dataContext.thisPlayerId;
		
		PlayerType enemyPlayerType = thisPlayerState.getPlayerType().getOpposite();
		_dataContext.playerStates.put(thisPlayerState.getPlayerType(), thisPlayerState);
		_dataContext.playerStates.put(enemyPlayerType, new PlayerState(enemyPlayerType));
		
		_dataContext.map = _dataContext.mapFactory.createMap(gameInfo.mapId);
		
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				Gdx.graphics.setTitle("playerType = " + thisPlayerState.getPlayerType());
				_game.setScreen(new GameScreen(_game, _dataContext));
			}
		});
	}
}
