package pl.wiocha.screens;

import com.badlogic.gdx.Game;

import pl.wiocha.DataContext;
import pl.wiocha.utils.GlobalTime;
import pl.wiocha.utils.callbacks.VoidCallback;

public class MenuScreen extends BaseScreen {
	private DataContext _dataContext;
	
	public MenuScreen(Game game, DataContext dataContext) {
		super(game, dataContext.batch);
		_dataContext = dataContext;
		
		GlobalTime.waitAndDoOnce(1f, new VoidCallback() {

			@Override
			public void onCallBack() {
				_game.setScreen(new ConnectingScreen(_game, _dataContext));
			}
			
		});
	}
}
