package pl.wiocha.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class BaseScreen implements Screen {
	protected final Game _game;
	protected final Stage _basicStage;
	
	
	protected BaseScreen(Game game, SpriteBatch batch) {
		_game = game;
		_basicStage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);
	}
	
	@Override
	public void render(float deltaTime) {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}
}
