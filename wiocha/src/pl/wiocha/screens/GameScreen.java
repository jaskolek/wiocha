package pl.wiocha.screens;

import static aurelienribon.tweenengine.TweenEquations.easeInExpo;
import static aurelienribon.tweenengine.TweenEquations.easeInOutSine;
import static aurelienribon.tweenengine.TweenEquations.easeInQuad;
import static aurelienribon.tweenengine.TweenEquations.easeInSine;
import static aurelienribon.tweenengine.TweenEquations.easeOutExpo;
import static aurelienribon.tweenengine.TweenEquations.easeOutQuart;
import static com.badlogic.gdx.math.Interpolation.sineIn;
import static com.badlogic.gdx.math.Interpolation.sineOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import java.util.HashMap;

import pl.wiocha.Assets;
import pl.wiocha.DataContext;
import pl.wiocha.UnitParametersManager;
import pl.wiocha.actors.BoardActor;
import pl.wiocha.actors.BoardStage;
import pl.wiocha.actors.ButtonsBarActor;
import pl.wiocha.actors.FieldActor;
import pl.wiocha.actors.ShadowActor;
import pl.wiocha.actors.SpawnUnitButton;
import pl.wiocha.actors.UnitActor;
import pl.wiocha.enums.Constants;
import pl.wiocha.enums.ContentTables;
import pl.wiocha.enums.PlayerType;
import pl.wiocha.enums.UnitType;
import pl.wiocha.listeners.BoardListener;
import pl.wiocha.logic.AttackUnitSimulationResult;
import pl.wiocha.logic.CharacterMoveSimulationResult;
import pl.wiocha.logic.HitUnitSimulationResult;
import pl.wiocha.logic.MapPath;
import pl.wiocha.logic.PlayerState;
import pl.wiocha.logic.SpawnUnitSimulationResult;
import pl.wiocha.model.Field;
import pl.wiocha.model.Map;
import pl.wiocha.model.Unit;
import pl.wiocha.model.UnitParameters;
import pl.wiocha.utils.AxialPosition;
import pl.wiocha.utils.Content;
import pl.wiocha.utils.GlobalTime;
import pl.wiocha.utils.TimelineBuilder;
import pl.wiocha.utils.ToastBox;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.TweenCallback;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class GameScreen extends BaseScreen {
	private BitmapFont _debugFont;
	private Stage _GUIStage;
	private BoardStage _boardStage;
	private OrthographicCamera _boardCamera;

	private InputMultiplexer _inputMultiplexer;

	private BoardActor _board;

	private Table _GUINotifications; // appears on very top
	// private Table _GUITopMenu;
	// private Table _GUIAttackerTopMenu;
	private ButtonsBarActor _spawnButtonsBar;
	private Table _GUIDefenderTopMenu;
	private Table _statsTable;
	private Table _devConsoleTable;

	private Label _currentTurnSidePlayerId;
	private Label _actionPointsValueLabel;
	private Label _goldValueLabel;
	private Label _manaValueLabel;

	private DataContext _dataContext;

	public enum GameState {
		DEFAULT,
		SPAWN,
		ANIMATION
	}

	private GameState _gameState = GameState.DEFAULT;


	public GameScreen(Game game, DataContext dataContext) {
		super(game, dataContext.batch);
		_dataContext = dataContext;
		_debugFont = new BitmapFont();
		
		_inputMultiplexer = new InputMultiplexer();

		float width = Gdx.graphics.getWidth();
		float height = Gdx.graphics.getHeight();

		_GUIStage = _basicStage;
		
		Skin skin = _dataContext.skin;

		_board = new BoardActor(dataContext.map);
		_board.setPosition(0, 32);
		_boardStage = new BoardStage(_board, width, height, false, dataContext.batch);
		_boardCamera = (OrthographicCamera) _boardStage.getCamera();

		_inputMultiplexer.addProcessor(_GUIStage);
		_inputMultiplexer.addProcessor(_boardStage.getInputProcessor());

		Gdx.input.setInputProcessor(_inputMultiplexer);

		_boardStage.addBoardListener(_boardDefaultListener);
		_boardStage.addBoardListener(_boardSpawnListener);

		//
		_GUINotifications = new Table();
		_GUINotifications.setFillParent(true);
		_GUIStage.addActor(_GUINotifications);

		_GUINotifications.addActor(ToastBox.instance);
		_GUINotifications.pack();


		setupSpawnMenu();
		setupDefenderTopMenu();

		// "GUI": player state stats etc.
		_statsTable = new Table();
		_statsTable.top().right().pad(5).setFillParent(true);
		_GUIStage.addActor(_statsTable);

		_currentTurnSidePlayerId = new Label("", skin);
		_statsTable.add(new Label("Turn:", skin)).left().align(Align.right).padRight(10);
		_statsTable.add(_currentTurnSidePlayerId).minWidth(20).left().row();

		_actionPointsValueLabel = new Label("", skin);
		_statsTable.add(new Label("Action Points:", skin)).left().align(Align.right).padRight(10);
		_statsTable.add(_actionPointsValueLabel).minWidth(20).left().row();

		_goldValueLabel = new Label("", skin);
		_statsTable.add(new Label("Gold:", skin)).left().align(Align.right).padRight(10);
		_statsTable.add(_goldValueLabel).minWidth(20).left().row();

		_manaValueLabel = new Label("", skin);
		_statsTable.add(new Label("Mana:", skin)).left().align(Align.right).padRight(10);
		_statsTable.add(_manaValueLabel).minWidth(20).left().row();

		_statsTable.pack();

		updatePlayerStatsGUI();
		updateTopMenu();

		// "GUI": developer console
		setupDeveloperConsoleTable();
	}

	private void setupSpawnMenu() {
		// create GUI
		_spawnButtonsBar = new ButtonsBarActor();
		_GUIStage.addActor(_spawnButtonsBar);

		Drawable drawable = new TextureRegionDrawable(new TextureRegion(Assets.Textures.ADD_UNITS_BUTTON));
		Drawable drawableChecked = new TextureRegionDrawable(new TextureRegion(Assets.Textures.ADD_UNITS_BUTTON_CHECKED));

		Button toggleButton = new Button(drawable, null, drawableChecked);
		_spawnButtonsBar.setToggleButton(toggleButton);

		for (Content.Table.Row row : Assets.Content.getRowsByRefColumn(ContentTables.Unit, "side", "attack")) {
			SpawnUnitButton button = new SpawnUnitButton(_dataContext.unitParametersManager.get(row.getString("id")));
			_spawnButtonsBar.addButton(button);
			button.addListener(_spawnButtonTouchListener);
		}
	}

	private void setupDefenderTopMenu() {
		// TODO
		_GUIDefenderTopMenu = new Table();
		_GUIStage.addActor(_GUIDefenderTopMenu);
	}

	private void updateTopMenu() {
		if (_dataContext.currentPlayerId == PlayerType.Defense) {
			float waitTime = 0;
			if (_spawnButtonsBar.isOpened()) {
				_spawnButtonsBar.close();
				waitTime = ButtonsBarActor.OPEN_ANIMATION_TIME + 0.25f; // wait for closing
																		// _spawnButtonsBar
			}
			//@formatter:off
			TimelineBuilder timeline = TimelineBuilder.createSequence()
			.beginSequence().setActor(_spawnButtonsBar)
				.wait(waitTime)
				.moveToY(_GUIStage.getHeight(), 1f, easeOutExpo)
			.end();
			//@formatter:on
			timeline.start();
		}
		else {
			//@formatter:off
			TimelineBuilder timeline = TimelineBuilder.createSequence()
			.beginSequence().setActor(_spawnButtonsBar)
				.moveToY(_GUIStage.getHeight() - _spawnButtonsBar.getHeight(), 1f, easeOutExpo)
			.end();
			//@formatter:on
			timeline.start();
		}
	}

	private void setupDeveloperConsoleTable() {
		_devConsoleTable = new Table();
		_devConsoleTable.setFillParent(true);
		_devConsoleTable.setVisible(false);
		_GUIStage.addActor(_devConsoleTable);

		_inputMultiplexer.addProcessor(new InputAdapter() {
			@Override
			public boolean keyDown(int keycode) {
				boolean keyHandled = true;

				if (keycode == Keys.C) {
					_devConsoleTable.setVisible(!_devConsoleTable.isVisible());

					if (_devConsoleTable.isVisible()) {
						ToastBox.show("Developer Console");
					}
				}
				else if (keycode == Keys.SHIFT_LEFT) {
					GlobalTime.timeScaleFactor = 0.2f;
				}
				else {
					keyHandled = false;
				}

				return keyHandled;
			}

			@Override
			public boolean keyUp(int keycode) {
				if (keycode == Keys.SHIFT_LEFT) {
					GlobalTime.timeScaleFactor = 1.0f;
					return true;
				}

				return super.keyUp(keycode);
			}
		});

		// Setup buttons
		TextButton addActionPointsButton = new TextButton("Add 10 AP", _dataContext.skin);
		_devConsoleTable.add(addActionPointsButton).row();
		addActionPointsButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				final PlayerState currentPlayerState = _dataContext.getCurrentPlayerState();
				
				currentPlayerState.setActionPoints(currentPlayerState.getActionPoints() + 10);
				updatePlayerStatsGUI();
			}
		});

		TextButton changeTurnSideButton = new TextButton("Change turn side", _dataContext.skin);
		_devConsoleTable.add(changeTurnSideButton);
		changeTurnSideButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				_dataContext.currentPlayerId = _dataContext.currentPlayerId.getOpposite();
				_board.unselectUnit();

				updatePlayerStatsGUI();
				updateTopMenu();
			}
		});
	}

	@Override
	public void dispose() {
		super.dispose();

		_GUIStage.dispose();
		_boardStage.dispose();
		_GUIStage = null;
		_boardStage = null;
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		_GUIStage.setViewport(width, height);
		_boardStage.setViewport(width, height);

		_spawnButtonsBar.setSize(width, 64);
		if (_dataContext.currentPlayerId == PlayerType.Attack) {
			_spawnButtonsBar.setPosition(0, height - _spawnButtonsBar.getHeight());
		}
		else {
			_spawnButtonsBar.setPosition(0, height);
		}
		// _spawnButtonsBar.setPosition(0, height - _spawnButtonsBar.getHeight());
	}

	@Override
	public void render(float deltaTime) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		_boardStage.act(deltaTime);
		_GUIStage.act(deltaTime);

		_boardStage.draw();
		_GUIStage.draw();


		// SpriteBatch batch = _GUIStage.getSpriteBatch();
		// batch.begin();
		// _debugFont.draw(batch, String.valueOf(x), 10, 100);
		// _debugFont.draw(batch, String.valueOf(y), 10, 80);
		// batch.end();

		// Table.drawDebug(_GUIStage);


		super.render(deltaTime);
	}

	private void updatePlayerStatsGUI() {
		final PlayerState currentPlayerState = _dataContext.getCurrentPlayerState(); 
		
		_currentTurnSidePlayerId.setText(currentPlayerState.getPlayerType().name());
		_actionPointsValueLabel.setText(String.valueOf(currentPlayerState.getActionPoints()));
		_goldValueLabel.setText(String.valueOf(currentPlayerState.getGold()));
		_manaValueLabel.setText(String.valueOf(currentPlayerState.getMana()));
	}

	private void animateAttackUnit(AttackUnitSimulationResult result, final UnitActor attackerUnitActor, final UnitActor defenderUnitActor, TweenCallback callback) {
		final FieldActor attackerFieldActor = _board.getFieldForUnitActor(attackerUnitActor);
		final FieldActor defenderFieldActor = _board.getFieldForUnitActor(defenderUnitActor);

		Vector2 startPosition = new Vector2(attackerUnitActor.getX(), attackerUnitActor.getY());
		Vector2 endPosition = new Vector2(defenderUnitActor.getX(), defenderUnitActor.getY());

		Vector2 startAttackPosition = startPosition.cpy().lerp(endPosition, 0.5f);
		float attackerRotation = endPosition.cpy().sub(startPosition).angle();
		float defenderRotation = attackerRotation + 180;
		
		float attackerRotationTime = 0.002f * Math.abs(attackerRotation);
		float defenderRotationTime = 0.002f * Math.abs(defenderRotation);
		
		float hitTime = 0.2f;// 0.75f / result.hits.size;
		
		//@formatter:off
		TimelineBuilder timeline = TimelineBuilder.createSequence();
		timeline.beginParallel()
			.beginSequence().setActor(attackerUnitActor)
				.rotateTo(attackerRotation, attackerRotationTime)
			.end();
		
		//do not rotate buildings!
		if(result.defender.parameters.type != UnitType.Building){
			timeline.beginSequence().setActor(defenderUnitActor)
				.rotateTo(defenderRotation, defenderRotationTime)
			.end();
		}
		timeline.end();
		
		
		timeline.beginSequence().setActor(attackerUnitActor)
			.moveTo(startAttackPosition.x, startAttackPosition.y, 0.5f)
		.end();
		;
		for (HitUnitSimulationResult hitResult: result.hits){
			timeline.beginParallel()
				.beginSequence().setActor(defenderFieldActor)
					.wait(hitTime)
					.colorTo(1, 0, 0, hitTime * 0.1f)
					.colorTo(defenderFieldActor.getBaseColor(), hitTime * 0.9f)
				.end()
				.beginSequence().setActor(attackerUnitActor)
					.beginParallel()
						.moveTo(endPosition.x, endPosition.y, hitTime, easeInExpo)
					.end()
					
					.moveTo(startAttackPosition.x, startAttackPosition.y, hitTime)
				.end()
			.end();
		}
		//@formatter:on

		timeline.beginSequence().setActor(attackerUnitActor)
			.moveTo(startPosition.x, startPosition.y, 0.5f)
		.end();

		//animate dead
		if(result.defender.isDead()){
			timeline.beginSequence()
				.beginSequence().setActor(defenderFieldActor)
					.alphaTo(0, 0.2f)
				.end()
				.beginParallel().setActor(defenderUnitActor)
					.moveByY(-10, 1f, easeInQuad)
					.scaleTo(0.2f, 1f)
					.alphaTo(0, 1f)
				.end()
				.call(new TweenCallback() {
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						_board.removeActor(defenderUnitActor);
					}
				})
				.beginSequence().setActor(defenderFieldActor)
					.alphaTo(1, 0.2f)
				.end()
			.end();
		}
		
		timeline.call(callback);
		timeline.start();
	}

	private void animateSpawnUnit(UnitActor unitActor, TweenCallback callback) {
		TimelineBuilder timeline = TimelineBuilder.createSequence().setActor(unitActor);
		Unit unit = unitActor.getUnit();
		Field targetField = unit.getParentField();
		FieldActor targetFieldActor = _board.getFieldActor(targetField.getPosition());

		Vector2 targetCenter = _board.positionToBoardCoordinates(targetField.getPosition());
		Vector2 targetPosition = targetCenter.cpy().sub(unitActor.getWidth() * 0.5f, unitActor.getHeight() * 0.5f);

		float startScale = 6f;
		float fallHeight = 100;
		float fallTime = 1f;
		float bounceTime = 0.2f;

		Vector2 startPosition = targetPosition.cpy().add(0, fallHeight);
		Vector2 center = new Vector2(_board.getWidth() * 0.5f, _board.getHeight() * 0.5f);
		float angle = center.cpy().sub(targetCenter).angle();

		unitActor.setPosition(startPosition.x, startPosition.y);
		unitActor.scale(startScale);
		unitActor.setRotation(angle);

		final ShadowActor shadow = new ShadowActor(unitActor.getTexture());
		shadow.setSize(unitActor.getWidth(), unitActor.getHeight());
		shadow.setPosition(targetPosition.x, targetPosition.y);
		shadow.setScale(startScale);
		shadow.setAlpha(0.2f);
		shadow.setRotation(angle);

		_board.addActorBefore(unitActor, shadow);
		//@formatter:off
		timeline.beginSequence()
			.beginParallel()
				.beginParallel().setActor(unitActor)
					.scaleTo(1, fallTime)
					.moveTo(targetPosition.x, targetPosition.y, fallTime, easeInQuad)
				.end()
				.beginParallel().setActor(shadow)
					.scaleTo(1, fallTime)
					.alphaTo(1, fallTime, easeInQuad)
				.end()
			.end()
			.call(new TweenCallback() {
				@Override
				public void onEvent(int type, BaseTween<?> source) {
					_board.removeActor(shadow);
				}
			})
			.beginParallel()
				.beginSequence().setActor(unitActor)
					.beginParallel()
						.scaleTo(0.8f, bounceTime, easeOutQuart)
						.moveBy(0, -5f, bounceTime, easeOutQuart)
					.end()
					.beginParallel()
						.moveBy(0, 5f, bounceTime*2, easeInOutSine)
						.scaleTo(1, bounceTime*2, easeInOutSine)
					.end()
				.end()
				.beginSequence().setActor(targetFieldActor)
					.beginParallel()
						.scaleTo(0.8f, bounceTime, easeOutQuart)
						.moveBy(0, -5f, bounceTime, easeOutQuart)
					.end()
					.beginParallel()
						.moveBy(0, 5f, bounceTime*2, easeInOutSine)
						.scaleTo(1, bounceTime*2, easeInOutSine)
					.end()
				.end()
			.end()
			.call(callback)
		.end();
		//@formatter:on
		timeline.start();
	}

	/**
	 * Moves only on straight path.
	 * 
	 * TODO block touch when animation is being played
	 */
	private void animateMoveUnit(UnitActor unitActor, MapPath path) {
		float scaleTime = Constants.Animations.Unit.JumpTimeFactor / 4;

		FieldActor startingFieldActor = _board.getFieldActor(path.getStartingPosition());
		FieldActor finalFieldActor = _board.getFieldActor(path.getFinalPosition());

		TimelineBuilder timeline = TimelineBuilder.createSequence();

		for (int i = 0; i < path.getLength(); ++i) {
			AxialPosition targetAxialPosition = path.getNthPosition(i);
			FieldActor fieldActor = _board.getFieldActor(targetAxialPosition);

			timeline.setActor(fieldActor)
					.alphaTo(0.7f, 0.1f, easeInSine);
		}

		for (int i = 1; i < path.getLength(); ++i) {
			AxialPosition previousAxialPosition = path.getNthPosition(i - 1);
			AxialPosition targetAxialPosition = path.getNthPosition(i);
			Vector2 targetPosition = _board.positionToBoardCoordinates(targetAxialPosition).sub(unitActor.getWidth() / 2, unitActor.getHeight() / 2);

			FieldActor targetFieldActor = _board.getFieldActor(targetAxialPosition);

			float rotation = previousAxialPosition.getRotationToPosition(targetAxialPosition);
			//@formatter:off
			timeline.beginParallel().setActor(unitActor)
				.rotateTo(rotation, scaleTime)
				.scaleTo(Constants.Animations.Unit.JumpScaleFactor, scaleTime*2, easeInSine)
				.moveTo(targetPosition.x, targetPosition.y, scaleTime*4, easeInSine)
				.beginSequence()
					.wait(scaleTime*3)
					.beginParallel()
						.setActor(targetFieldActor)
							.beginSequence()
								.beginParallel()
									.alphaTo(0.5f, scaleTime, easeInSine)
									.moveThroughCatmulRomWaypoints(0.2f, 2, 0, 0, 2, -1, 0).targetRelative(0, 0)
								.end()
								.wait(scaleTime)
								.alphaTo(1f, scaleTime, easeInSine)
							.end()
						.setActor(unitActor)
							.scaleTo(1.0f, scaleTime, easeInSine)
					.end()
				.end()
			.end();
			//@formatter:on
		}

		//@formatter:off
		timeline.setActor(startingFieldActor)
			.alphaTo(1f, 0.2f, easeInSine)
		.start();
		
		// Shake final field on start
		TimelineBuilder.createSequence()
			.beginCatmulRomWaypoints(finalFieldActor, 0.5f)
				.waypoint(5, 0)
				.waypoint(0, 3)
				.waypoint(-3, 0)
				.targetRelative(0, 0)
		.start();
		//@formatter:on
	}


	// ///////////////////////////////////////
	// Listeners
	// ///////////////////////////////////////

	private final BoardListener _boardDefaultListener = new BoardListener() {
		@Override
		public void unitTapped(UnitActor tappedUnitActor) {
			if (_gameState != GameState.DEFAULT)
				return;
			UnitActor selectedUnitActor = _board.getSelectedUnit();
			Unit tappedUnit = tappedUnitActor.getUnit();

			if (tappedUnit.getOwner() == _dataContext.currentPlayerId) {
				// tap on my own unit
				_board.unselectUnit();
				if (selectedUnitActor != tappedUnitActor) {
					_board.selectUnit(tappedUnitActor);
				}
			}
			else { // tap on opponent unit
				if (selectedUnitActor != null) {
					Unit selectedUnit = selectedUnitActor.getUnit();
					AxialPosition selectedPosition = selectedUnit.getParentField().getPosition();
					AxialPosition tappedPosition = tappedUnit.getParentField().getPosition();

					final PlayerState playerState = _dataContext.getCurrentPlayerState();
					final AttackUnitSimulationResult attackResult = new AttackUnitSimulationResult(selectedUnit, tappedUnit);
					int neededActionPoints = 2;
					if (playerState.getActionPoints() >= neededActionPoints) {
						if (selectedPosition.getDistance(tappedPosition) <= 1) {
							// attack!
							attackResult.actionPointsCost = neededActionPoints;

							// simulateAttack
							for (int i = 0; i < attackResult.attacker.parameters.attack; ++i) {
								// for (int i = 0; i < 3; ++i) {
								int attack = MathUtils.random(attackResult.attacker.parameters.attack);
								int defense = MathUtils.random(attackResult.attacker.parameters.defense);

								int hitPointsTaken;
								if (attack >= defense) {
									hitPointsTaken = 1;
								}
								else {
									hitPointsTaken = 0;
								}
								HitUnitSimulationResult hitResult = new HitUnitSimulationResult(hitPointsTaken);
								attackResult.hits.add(hitResult);
								attackResult.defender.takeHitPoints(hitPointsTaken);
								if (attackResult.defender.isDead()) {
									
									break;
								}
							}
							playerState.takeActionPoints(attackResult.actionPointsCost);
							_board.unselectUnit();
							// animate
							_gameState = GameState.ANIMATION;
							
							animateAttackUnit(attackResult, selectedUnitActor, tappedUnitActor, new TweenCallback() {
								@Override
								public void onEvent(int type, BaseTween<?> source) {
									if( attackResult.defender.isDead() ){
										_dataContext.map.removeUnit(attackResult.defender);
									}
									_gameState = GameState.DEFAULT;
								}
							});
						}
						else {
							ToastBox.show(selectedUnit.parameters.fullName + " can attack only 1 field away.");
						}
					}
					else {
						ToastBox.show("Needed Action Points: " + neededActionPoints);
					}

				}
				else {
					// can't check opponent's unit
					FieldActor fieldActor = _board.getFieldForUnitActor(tappedUnitActor);
					Action shakeAction = sequence(
							moveBy(-5, 0, 0.05f, sineOut),
							moveBy(5, 0, 0.05f, sineIn),
							moveBy(5, 0, 0.05f, sineOut),
							moveBy(-5, 0, 0.05f, sineIn)
							);
					fieldActor.addAction(shakeAction);
				}
			}
		}

		@Override
		public void fieldTapped(FieldActor fieldActor) {
			if (_gameState != GameState.DEFAULT)
				return;

			UnitActor unitActor = _board.getSelectedUnit();

			if (unitActor == null)
				return;

			Unit unit = unitActor.getUnit();

			if (unit.getOwner() == _dataContext.currentPlayerId && unit.parameters.type == UnitType.Character) {
				final PlayerState playerState = _dataContext.getCurrentPlayerState();
				
				AxialPosition targetPosition = fieldActor.getField().getPosition();

				int stepCost = unit.parameters.actionPointsCostForStep;
				int maxSteps = playerState.getActionPoints() / stepCost;

				final Map map = _dataContext.map;
				CharacterMoveSimulationResult moveResult = new CharacterMoveSimulationResult(unit);

				if (map.canStandOnPosition(unit, targetPosition)) {
					if (map.isPositionInUnitRange(unit, targetPosition)) {
						MapPath path = map.findPath(unit, targetPosition);
						int neededActionPoints = path.getStepsCount() * stepCost;

						if (path.getStepsCount() <= maxSteps) {
							map.moveUnit(unit, targetPosition);
							moveResult.setPath(path);

							if (moveResult.wasUnitMoved) {
								playerState.takeActionPoints(neededActionPoints);
								_board.unselectUnit();
								updatePlayerStatsGUI();

								animateMoveUnit(unitActor, moveResult.takenPath);
							}
						}
						else {
							_board.animateBlinkPath(path);
							ToastBox.show("Needed Action Points: " + neededActionPoints + '.');
						}
					}
					else {
						ToastBox.show("That would be too far for " + unit.parameters.fullName + '.');
					}
				}
				else {
					ToastBox.show(unit.parameters.fullName + " can't stand on this field.");
				}
			}
		}
	};

	private final BoardListener _boardSpawnListener = new BoardListener() {
		@Override
		public void unitTapped(UnitActor unitActor) {
			if (_gameState != GameState.SPAWN)
				return;

			ToastBox.show("Can't spawn unit on another unit.");
		}

		public void fieldTapped(FieldActor fieldActor) {
			if (_gameState != GameState.SPAWN)
				return;

			AxialPosition touchedFieldPosition = fieldActor.getField().getPosition();
			SpawnUnitButton checkedButton = (SpawnUnitButton) _spawnButtonsBar.getChecked();

			if (checkedButton == null)
				return;

			UnitParameters unitParameters = checkedButton.getUnitParameters();
			Unit unit = new Unit(unitParameters, _dataContext.currentPlayerId);
			final PlayerState playerState = _dataContext.getCurrentPlayerState();

			int actionPoints = playerState.getActionPoints();
			int neededActionPoints = unit.parameters.actionPointsCostForSpawn;
			boolean canAfford = (actionPoints >= neededActionPoints);

			if (canAfford) {
				final Map map = _dataContext.map;
				// SpawnUnitSimulationResult spawnResult = _map.trySpawnUnit(unit,
				// touchedFieldPosition);
				SpawnUnitSimulationResult spawnResult = new SpawnUnitSimulationResult(unit);

				if (map.canStandOnPosition(unit, touchedFieldPosition)) {
					if (map.isAttackerSpawnAllowed(unit, touchedFieldPosition)) {
						map.setUnit(unit, touchedFieldPosition);
						spawnResult.actionPointsCost = neededActionPoints;
						spawnResult.wasUnitSpawned = true;

						if (spawnResult.wasUnitSpawned) {
							UnitActor spawnedUnitActor = _board.createUnitActor(unit);
							playerState.takeActionPoints(spawnResult.actionPointsCost);
							_spawnButtonsBar.toggle();
							_spawnButtonsBar.setDisabled(true);
							_gameState = GameState.ANIMATION;

							updatePlayerStatsGUI();

							animateSpawnUnit(spawnedUnitActor, new TweenCallback() {
								@Override
								public void onEvent(int type, BaseTween<?> source) {
									_spawnButtonsBar.setDisabled(false);
									_gameState = GameState.DEFAULT;
								}
							});
						}
					}
					else {
						ToastBox.show("Units can be spawned only on board borders.");
					}
				}
				else {
					ToastBox.show(unit.parameters.fullName + " can't be spawned on this field.");
				}
			}
			else {
				ToastBox.show("Needed Action Points: " + neededActionPoints + '.');
			}
		}
	};

	private final ChangeListener _spawnButtonTouchListener = new ChangeListener() {
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			SpawnUnitButton button = (SpawnUnitButton) actor;

			if (button.isChecked()) {
				_gameState = GameState.SPAWN;
				UnitParameters parameters = button.getUnitParameters();
				Unit unit = new Unit(parameters, PlayerType.Attack);

				_board.unselectUnit();
				_board.enableSpawnAllowedFields(unit);
			}
			else {
				_gameState = GameState.DEFAULT;
				_board.clearAllFields();
			}
		}
	};
}
