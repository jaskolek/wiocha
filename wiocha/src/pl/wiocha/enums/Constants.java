package pl.wiocha.enums;

public abstract class Constants {
	public abstract class BoardCamera {
		public static final float Acceleration = 400f;
		public static final float MaxSpeed = 400f;
		public static final float Friction = 800f;
	}
	
	public abstract class Board {
		public static final float FieldRadius = 32;
	}
	
	public abstract class Animations {
		public abstract class Unit {
			public static final float JumpTimeFactor = 0.4f;
			public static final float JumpScaleFactor = 1.25f;
		}
	}
}
