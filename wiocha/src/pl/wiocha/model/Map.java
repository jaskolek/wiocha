package pl.wiocha.model;

import pl.wiocha.logic.CharacterMoveSimulationResult;
import pl.wiocha.logic.MapPath;
import pl.wiocha.logic.SpawnUnitSimulationResult;
import pl.wiocha.logic.path_finders.AStarPathFinderStrategy;
import pl.wiocha.logic.path_finders.DummyPathFinderFieldAcceptPredicate;
import pl.wiocha.logic.path_finders.IPathFinderFieldAcceptPredicate;
import pl.wiocha.logic.path_finders.IPathFinderStrategy;
import pl.wiocha.utils.AxialPosition;

import com.badlogic.gdx.utils.Array;

public class Map {
	private int _rows;
	private int _cols;

	private Array<Array<Field>> _fields;
	private Array<Unit> _units;

	private static IPathFinderStrategy _pathFinder = new AStarPathFinderStrategy();

	/**
	 * creates rhombus map
	 * 
	 * @param rows
	 * @param cols
	 */
	public Map(int rows, int cols) {
		_units = new Array<Unit>(false, 16);

		_rows = rows;
		_cols = cols;

		_fields = new Array<Array<Field>>(rows);
		for (int row = 0; row < rows; ++row) {
			Array<Field> arrayRow = new Array<Field>(cols);

			for (int col = 0; col < cols; ++col) {
				arrayRow.add(null);
			}
			_fields.add(arrayRow);
		}
	}

	public int getRowsCount() {
		return _rows;
	}

	public int getColsCount() {
		return _cols;
	}

	public boolean isPositionInRange(AxialPosition position) {
		return isPositionValid(position.row, position.col);
	}


	/**
	 * returns all valid positions where distance between center <= N
	 * 
	 * @param center
	 * @param N
	 * @return
	 */
	public Array<AxialPosition> getPositionsInDistance(AxialPosition center, int N) {
		Array<AxialPosition> positions = new Array<AxialPosition>();
		for (AxialPosition position : center.getPositionsInDistance(N)) {
			if (isPositionValid(position.row, position.col)) {
				positions.add(position);
			}
		}

		return positions;
	}

	/**
	 * checks if position (row,col) is valid position on this map;
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public boolean isPositionValid(int row, int col) {
		boolean isInArrayRange = (row >= 0 && col >= 0 && row < _rows && col < _cols);
		boolean isFieldNull = true;
		if (isInArrayRange) {
			isFieldNull = (getField(row, col) == null);
		}

		return isInArrayRange && !isFieldNull;
	}

	public void setField(AxialPosition position, Field field) {
		setField(position.row, position.col, field);
	}

	public void setField(int row, int col, Field field) {
		_fields.get(row).set(col, field);
	}

	public Field getField(AxialPosition position) {
		return getField(position.row, position.col);
	}

	public Field getField(int row, int col) {
		return _fields.get(row).get(col);
	}

	public void addUnit(Unit unit) {
		_units.add(unit);
	}

	public Array<Unit> getUnits() {
		return _units;
	}

	public int getUnitsCount() {
		return _units.size;
	}


	/**
	 * Returns normalized width of board.
	 * 1 = lenght of hex side
	 * 
	 * @return
	 */
	public float getNormalizedWidth() {
		return (float) Math.sqrt(3) * (_cols + _rows * 0.5f - 0.5f);
	}

	/**
	 * Returns normalized height of board.
	 * 1 = lenght of hex side
	 * 
	 * @return
	 */
	public float getNormalizedHeight() {
		return 1.5f * _rows + 0.5f;
	}

	/**
	 * Sets new unit on map
	 * 
	 * @param unit
	 * @param targetPosition
	 */
	public void setUnit(Unit unit, AxialPosition targetPosition) {
		Field field = getField(targetPosition);

		unit.setParentField(field);
		field.addUnit(unit);
	}

	/**
	 * removes unit from map
	 * @param unit
	 */
	public void removeUnit(Unit unit) {
		Field field = unit.getParentField();

		field.removeUnit(unit);
		unit.setParentField(null);
		_units.removeValue(unit, true);
	}

	public AxialPosition getUnitPosition(Unit unit) {
		Field field = unit.getParentField();

		return field.getPosition();
	}

	public MapPath findPath(final Unit unit, AxialPosition targetPosition) {
		AxialPosition startPosition = getUnitPosition(unit);

		IPathFinderFieldAcceptPredicate fieldPredicate = new IPathFinderFieldAcceptPredicate() {
			@Override
			public boolean checkField(Field field) {
				boolean isFieldBlockedForUnit = unit.parameters.blockedFieldTypes.contains(field.type, true);
				boolean isFieldEmpty = field.getUnits().size == 0;

				return !isFieldBlockedForUnit && isFieldEmpty;
			}
		};

		return findPath(startPosition, targetPosition, fieldPredicate);
	}

	public MapPath findPath(AxialPosition startPosition, AxialPosition targetPosition) {
		return findPath(startPosition, targetPosition, new DummyPathFinderFieldAcceptPredicate());
	}


	public boolean isPositionInUnitRange(Unit unit, AxialPosition targetPosition) {
		Array<AxialPosition> accessiblePositions = getAccessiblePositions(unit, unit.parameters.maxDistance);

		boolean canMakeMove = false;
		if (accessiblePositions.contains(targetPosition, false)) {
			canMakeMove = true;
		}

		return canMakeMove;
	}

	@Deprecated
	public CharacterMoveSimulationResult tryMoveUnit(Unit unit, AxialPosition targetPosition, int maxStepsCount) {
		// assert (maxStepsCount <= unit.parameters.maxDistance);

		CharacterMoveSimulationResult result = new CharacterMoveSimulationResult(unit);

		if (isPositionInUnitRange(unit, targetPosition)) {
			MapPath path = findPath(unit, targetPosition);

			if (path.getStepsCount() <= maxStepsCount) {
				moveUnit(unit, targetPosition);
				result.setPath(path);
			}
		}

		return result;
	}

	private MapPath findPath(AxialPosition startPosition, AxialPosition targetPosition, IPathFinderFieldAcceptPredicate fieldPredicate) {
		Array<AxialPosition> foundPath = _pathFinder.findPath(this, startPosition, targetPosition, fieldPredicate);

		return new MapPath(foundPath);
	}

	public void moveUnit(Unit unit, AxialPosition targetPosition) {
		moveUnit(unit, getField(targetPosition));
	}

	public void moveUnit(Unit unit, Field field) {
		Field oldField = unit.getParentField();
		oldField.removeUnit(unit);

		unit.setParentField(field);
		field.addUnit(unit);
	}

	@Deprecated
	public SpawnUnitSimulationResult trySpawnUnit(Unit unit, AxialPosition position) {
		SpawnUnitSimulationResult result = new SpawnUnitSimulationResult(unit);
		int cost = unit.parameters.actionPointsCostForSpawn;

		if (canSpawnUnit(unit, position)) {
			setUnit(unit, position);
			result.actionPointsCost = cost;
			result.wasUnitSpawned = true;
		}
		else {
			result.wasUnitSpawned = false;
		}

		return result;
	}

	/**
	 * returns collection of positions where attacker can spawn Unit unit
	 * 
	 * @param unit
	 * @return
	 */
	public Array<AxialPosition> getSpawnAllowedFields(Unit unit) {
		Array<AxialPosition> positions = new Array<AxialPosition>(false, 16);

		for (int row = 0; row < _rows; ++row) {
			for (int col = 0; col < _cols; ++col) {
				if (isPositionValid(row, col)) {
					AxialPosition position = new AxialPosition(row, col);
					if (isAttackerSpawnAllowed(unit, position)
							&& canStandOnPosition(unit, position)
							&& isFieldEmpty(position)) {
						positions.add(position);
					}
				}
			}
		}
		return positions;
	}

	public boolean isAttackerSpawnAllowed(Unit unit, AxialPosition position) {
		Field field = getField(position);

		return field.isAttackerSpawnAllowed;
	}

	@Deprecated
	public boolean canSpawnUnit(Unit unit, AxialPosition position) {
		boolean canStandOnPosition = canStandOnPosition(unit, position);
		Field field = getField(position);

		return canStandOnPosition && field.isAttackerSpawnAllowed && field.getUnits().size == 0;
	}

	public boolean isFieldEmpty(AxialPosition position) {
		Field field = getField(position);
		return field.getUnits().size == 0;
	}

	public boolean canStandOnPosition(Unit unit, AxialPosition position) {
		Field field = getField(position);

		return !unit.parameters.blockedFieldTypes.contains(field.type, true);
	}


	public Array<AxialPosition> getAccessiblePositions(Unit unit, int distance) {
		Array<AxialPosition> visited = new Array<AxialPosition>(false, 16);
		Array<Array<AxialPosition>> borders = new Array<Array<AxialPosition>>(distance + 1);
		Field field = unit.getParentField();
		AxialPosition position = field.getPosition();

		borders.add(new Array<AxialPosition>(1));
		borders.get(0).add(position);

		Array<Field.Type> blockedTypes = unit.parameters.blockedFieldTypes;

		for (int i = 0; i < distance; ++i) {
			Array<AxialPosition> border = borders.get(i);
			borders.add(new Array<AxialPosition>());

			for (AxialPosition borderPosition : border) {
				Array<AxialPosition> neighbors = borderPosition.getNeighbors();

				for (AxialPosition neighbor : neighbors) {
					if (isPositionInRange(neighbor)) {
						Field neighborField = getField(neighbor);
						if (neighborField.getUnits().size == 0
								&& !blockedTypes.contains(neighborField.type, false)
								&& !visited.contains(neighbor, false)) {
							visited.add(neighbor);
							borders.get(i + 1).add(neighbor);
						}
					}
				}
			}
		}

		return visited;
	}
}
