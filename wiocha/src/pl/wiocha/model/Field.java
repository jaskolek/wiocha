package pl.wiocha.model;

import com.badlogic.gdx.utils.Array;

import pl.wiocha.utils.AxialPosition;

public class Field {
	public enum Type {
		GRASS, WATER, GROUND
	};

	private AxialPosition _position;
	private Array<Unit> _units;

	public Type type = Type.GRASS;
	public boolean isAttackerSpawnAllowed = false;
	
	
	public Field(AxialPosition position) {
		_position = position;
		_units = new Array<Unit>(false, 1);
	}

	public Field(int row, int col) {
		this(new AxialPosition(row, col));
	}

	/**
	 * returns copy of position to avoid changing by mistake
	 * 
	 * @return
	 */
	public AxialPosition getPosition() {
		return _position.cpy();
	}

	public Array<Unit> getUnits() {
		return _units;
	}

	public void addUnit(Unit unit) {
		_units.add(unit);
	}

	public void removeUnit(Unit unit) {
		_units.removeValue(unit, false);
	}
}
