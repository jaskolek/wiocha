package pl.wiocha.model;

import pl.wiocha.enums.PlayerType;

public class Unit {
	public UnitParameters parameters;
	private PlayerType _owner;
	private Field _parentField;
	private int _hitPoints;


	public Unit(UnitParameters parameters, PlayerType owner) {
		this.parameters = parameters;
		_hitPoints = parameters.maxHitPoints;
		_owner = owner;
	}

	public Field getParentField() {
		return _parentField;
	}

	public void setParentField(Field field) {
		_parentField = field;
	}

	public PlayerType getOwner() {
		return _owner;
	}

	public void setOwnerId(PlayerType owner) {
		_owner = owner;
	}

	public void takeHitPoints(int hitPoints) {
		_hitPoints -= hitPoints;
	}
	
	public boolean isDead(){
		return _hitPoints <= 0;
	}
}
