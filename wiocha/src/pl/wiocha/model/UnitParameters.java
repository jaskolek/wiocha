package pl.wiocha.model;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.enums.UnitType;

import com.badlogic.gdx.utils.Array;

public class UnitParameters {
	public String img;
	public UnitType type;
	public PlayerType side;
	public String spawnButtonImg;
	public String spawnButtonImgChecked;
	public String fullName;
	public int maxDistance;				//max count of path fields to walk in turn
	public int actionPointsCostForStep;
	public int actionPointsCostForSpawn;
	
	public int attack;
	public int defense;
	public int maxHitPoints;
	
	public Array<Field.Type> blockedFieldTypes = new Array<Field.Type>();
}
