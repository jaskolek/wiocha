package pl.wiocha;

import pl.wiocha.enums.ContentTables;
import pl.wiocha.enums.UnitType;
import pl.wiocha.model.Field;
import pl.wiocha.model.UnitParameters;
import pl.wiocha.utils.Content;

public class UnitParametersManager {

	public UnitParameters get(String name) {
		final Content.Table.Row unitParamsRow = Assets.Content.getFirstRowByRefColumn(ContentTables.Unit, "id", name); 

		UnitParameters parameters = new UnitParameters();
		parameters.img = "data/" + name + ".png";
		parameters.spawnButtonImg = "data/spawn_" + name + "_button.png";
		parameters.spawnButtonImgChecked = "data/spawn_" + name + "_button_checked.png";
		
		if (unitParamsRow != null) {
			parameters.fullName = unitParamsRow.getString("full_name");
			parameters.type = unitParamsRow.getEnum("type", UnitType.class);
			parameters.maxDistance = unitParamsRow.getInt("max_distance");
			parameters.actionPointsCostForStep = unitParamsRow.getInt("step_cost");
			parameters.actionPointsCostForSpawn = unitParamsRow.getInt("spawn_cost");
			parameters.attack = unitParamsRow.getInt("attack");
			parameters.defense = unitParamsRow.getInt("defense");
			parameters.maxHitPoints = unitParamsRow.getInt("max_hit_points");
			
			//Loading blocked fields;
			String[] stringBlockedTypes = unitParamsRow.getStringArray("blocked_fields");
			for( String stringBlockedType: stringBlockedTypes ){
				Field.Type type = Field.Type.valueOf(stringBlockedType.toUpperCase());
				parameters.blockedFieldTypes.add(type);
			}
		}
		
		return parameters;
	}

}