package pl.wiocha;

import pl.wiocha.screens.BaseScreen;
import pl.wiocha.screens.MenuScreen;
import pl.wiocha.utils.GlobalTime;
import pl.wiocha.utils.TimelineBuilder;
import pl.wiocha.utils.ToastBox;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Wiocha extends Game {

	private AssetManager _assetManager;
	private DataContext _dataContext;
	

	@Override
	public void create() {
		initAssetManager();
		
		_dataContext = new DataContext();
		_dataContext.batch = new SpriteBatch();
		ToastBox.instance = new ToastBox(_dataContext.skin);
		
		BaseScreen firstScreen = new MenuScreen(this, _dataContext);

		setScreen(firstScreen);
	}
	
	@Override
	public void render() {
		float deltaTime = Gdx.graphics.getDeltaTime();
		GlobalTime.deltaTime = deltaTime;
		
		TimelineBuilder.update(GlobalTime.getScaledDeltaTime());
		
		super.render();
	}



	private void initAssetManager() {
		_assetManager = new AssetManager();
		_assetManager.load("data/base_field.png", Texture.class);
		_assetManager.load("data/wizard.png", Texture.class);
		_assetManager.load("data/house_1.png", Texture.class);
		_assetManager.load("data/house_2.png", Texture.class);
		_assetManager.load("data/house_3.png", Texture.class);
		_assetManager.load("data/windmill.png", Texture.class);
		
		_assetManager.load("data/add_units_button.png", Texture.class);
		_assetManager.load("data/add_units_button_checked.png", Texture.class);
		
		_assetManager.load("data/bee.png", Texture.class);
		_assetManager.load("data/bug.png", Texture.class);
		_assetManager.load("data/cockroach.png", Texture.class);

		_assetManager.load("data/spawn_bee_button.png", Texture.class);
		_assetManager.load("data/spawn_bug_button.png", Texture.class);
		_assetManager.load("data/spawn_cockroach_button.png", Texture.class);
		_assetManager.load("data/spawn_bee_button_checked.png", Texture.class);
		_assetManager.load("data/spawn_bug_button_checked.png", Texture.class);
		_assetManager.load("data/spawn_cockroach_button_checked.png", Texture.class);

		_assetManager.finishLoading();
		Assets.init(_assetManager);

		Assets.Textures.BASE_FIELD = new TextureRegion(_assetManager.get("data/base_field.png", Texture.class));
		Assets.Textures.WIZARD = new TextureRegion(_assetManager.get("data/wizard.png", Texture.class));

		Assets.Textures.HOUSE_1 = new TextureRegion(_assetManager.get("data/house_1.png", Texture.class));
		Assets.Textures.HOUSE_2 = new TextureRegion(_assetManager.get("data/house_2.png", Texture.class));
		Assets.Textures.HOUSE_3 = new TextureRegion(_assetManager.get("data/house_3.png", Texture.class));
		Assets.Textures.WINDMILL = new TextureRegion(_assetManager.get("data/windmill.png", Texture.class));

		Assets.Textures.ADD_UNITS_BUTTON = _assetManager.get("data/add_units_button.png", Texture.class);
		Assets.Textures.ADD_UNITS_BUTTON_CHECKED = _assetManager.get("data/add_units_button_checked.png", Texture.class);

	}
}
