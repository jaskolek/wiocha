package pl.wiocha.actors;

import pl.wiocha.actors.FieldActor;
import pl.wiocha.enums.Constants;
import pl.wiocha.logic.MapPath;
import pl.wiocha.model.Field;
import pl.wiocha.model.Map;
import pl.wiocha.model.Unit;
import pl.wiocha.utils.AxialPosition;
import pl.wiocha.utils.TimelineBuilder;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class BoardActor extends Group {
	private Map _map;

	public static final float FIELD_RADIUS = Constants.Board.FieldRadius;

	private Array<Array<FieldActor>> _fields;
	private Array<UnitActor> _units;

	private FieldActor _selectedField = null;
	private Array<FieldActor> _accessibleFields;

	private UnitActor _selectedUnit = null;

	public BoardActor(Map map) {
		super();

		_accessibleFields = new Array<FieldActor>(false, 16);
		_map = map;

		int rows = map.getRowsCount();
		int cols = map.getColsCount();

		_fields = new Array<Array<FieldActor>>(rows);
		for (int row = 0; row < rows; ++row) {
			Array<FieldActor> fieldsRow = new Array<FieldActor>(cols);

			for (int col = 0; col < cols; ++col) {

				Field field = _map.getField(row, col);
				if (field != null) {
					FieldActor fieldActor = new FieldActor(field);

					fieldActor.setSize(FIELD_RADIUS * 2, FIELD_RADIUS * 2);
					Vector2 center = positionToBoardCoordinates(row, col);

					fieldActor.setX(center.x - FIELD_RADIUS);
					fieldActor.setY(center.y - FIELD_RADIUS);

					fieldsRow.add(fieldActor);
					addActor(fieldActor);
				}
				else {
					fieldsRow.add(null);
				}
			}

			_fields.add(fieldsRow);
		}

		_units = new Array<UnitActor>(false, map.getUnitsCount());
		for (int i = 0; i < map.getUnitsCount(); ++i) {
			Unit unit = map.getUnits().get(i);
			createUnitActor(unit);
		}

		setWidth(FIELD_RADIUS * _map.getNormalizedWidth());
		setHeight(FIELD_RADIUS * _map.getNormalizedHeight());
		updateUnitsPositions();
	}

	@Override
	public void act(float delta) {
		super.act(delta);
	}

	public void updateUnitsPositions() {
		for (int i = 0; i < _units.size; ++i) {
			UnitActor unitActor = _units.get(i);
			Unit unit = unitActor.getUnit();

			AxialPosition position = unit.getParentField().getPosition();
			Vector2 center = positionToBoardCoordinates(position);
			Vector2 bottomLeft = center.cpy().sub(unitActor.getWidth() / 2,
					unitActor.getHeight() / 2);

			unitActor.setPosition(bottomLeft.x, bottomLeft.y);
		}
	}

	public UnitActor getSelectedUnit() {
		return _selectedUnit;
	}

	/**
	 * creates unitActor based on Unit
	 * 
	 * @param unit
	 */
	public UnitActor createUnitActor(Unit unit) {
		UnitActor unitActor = new UnitActor(unit);
		unitActor.setSize(FIELD_RADIUS * 1.25f, FIELD_RADIUS * 1.25f);
		_units.add(unitActor);
		addActor(unitActor);

		return unitActor;
	}

	/**
	 * select unit and sets accesible fields getted by unit strategvy
	 * 
	 * @param unitActor
	 */
	public void selectUnit(UnitActor unitActor) {
		_selectedUnit = unitActor;
		_selectedUnit.setSelected(true);

		Unit unit = unitActor.getUnit();
		Field field = unit.getParentField();
		AxialPosition position = field.getPosition();

		// podswietlanie wszystkich pol na ktore mozemy sie
		// poruszyc

		Array<AxialPosition> accessiblePositions = _map.getAccessiblePositions(unit, unit.parameters.maxDistance);

		for (int i = 0; i < accessiblePositions.size; ++i) {
			AxialPosition accessiblePosition = accessiblePositions.get(i);
			FieldActor accessibleField = getFieldActor(accessiblePosition);

			accessibleField.setState(FieldActor.State.MOVEABLE);
			_accessibleFields.add(accessibleField);
		}

		Array<AxialPosition> attackablePositions = _map.getPositionsInDistance(position, 1);

		// highlight all attackable units
		for (AxialPosition attackablePosition : attackablePositions) {
			Field attackableField = _map.getField(attackablePosition);
			for (Unit attackableUnit : attackableField.getUnits()) {
				if (attackableUnit.getOwner() != unit.getOwner()) {
					getFieldActor(attackablePosition).setState(FieldActor.State.ATTACKABLE);
				}
			}
		}

	}

	public void unselectUnit() {
		if (_selectedUnit != null) {
			_selectedUnit.setSelected(false);

			clearAllFields();
			// clearAccessibleFields();
			_selectedUnit = null;
		}
	}

	@Deprecated
	public void clearAccessibleFields() {
		for (int i = 0; i < _accessibleFields.size; ++i) {
			_accessibleFields.get(i).setState(FieldActor.State.NORMAL);
		}
		_accessibleFields.clear();
	}

	public void enableSpawnAllowedFields(Unit unit) {
		for (AxialPosition position : _map.getSpawnAllowedFields(unit)) {
			getFieldActor(position).setState(FieldActor.State.SPAWNABLE);
		}
	}

	public void clearAllFields() {
		for (int row = 0; row < _map.getRowsCount(); ++row) {
			for (int col = 0; col < _map.getColsCount(); ++col) {
				FieldActor fieldActor = getFieldActor(row, col);
				if (fieldActor != null) {
					fieldActor.setState(FieldActor.State.NORMAL);
				}
			}
		}
	}

	public Vector2 positionToBoardCoordinates(AxialPosition position) {
		Vector2 coordinates = position.getNormalizedCenterCoordinates();
		coordinates.scl(FIELD_RADIUS);
		return coordinates;
	}

	/**
	 * gets hexagon center on board
	 * 
	 * @param position
	 * @return
	 */
	public Vector2 positionToBoardCoordinates(int row, int col) {
		return positionToBoardCoordinates(new AxialPosition(row, col));
	}

	public FieldActor getFieldActor(int row, int col) {
		return _fields.get(row).get(col);
	}

	public FieldActor getFieldActor(AxialPosition position) {
		return getFieldActor(position.row, position.col);
	}

	public FieldActor getFieldForUnitActor(UnitActor unitActor) {
		Unit unit = unitActor.getUnit();
		Field field = unit.getParentField();

		return getFieldActor(field.getPosition());
	}

	public void animateBlinkPath(MapPath path) {
		TimelineBuilder timeline = TimelineBuilder.createSequence();

		for (int j = 0; j < 2; ++j) {
			timeline.beginParallel();
			for (int i = 1, n = path.getLength(); i < n; ++i) {
				FieldActor fieldActor = getFieldActor(path.getNthPosition(i));
				timeline.alphaTo(fieldActor, 0.8f, 0.1f);
			}
			timeline.end()
					.wait(0.4f)
					.beginParallel();
			for (int i = 1, n = path.getLength(); i < n; ++i) {
				FieldActor fieldActor = getFieldActor(path.getNthPosition(i));
				timeline.alphaTo(fieldActor, 1f, 0.1f);
			}
			timeline.end();
		}

		timeline.start();
	}
}