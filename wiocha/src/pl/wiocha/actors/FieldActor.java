package pl.wiocha.actors;

import pl.wiocha.Assets;
import pl.wiocha.model.Field;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public class FieldActor extends Actor {
	private Field _field;
	
	public enum State{
		NORMAL,
		MOVEABLE,
		SPAWNABLE,
		ATTACKABLE
	};
	
	private Color _baseColor;
	private State _state = State.NORMAL;
	
	public FieldActor(Field _field) {
		super();
		this._field = _field;
		switch (_field.type) {
			case GRASS:
				_baseColor = new Color(0, 1, 0, 1);
				break;
			case WATER:
				_baseColor = new Color(0, 0.25f, 0.5f, 1f);
				break;
			case GROUND:
				_baseColor = new Color(0.5f, 0.2f, 0, 1);
				break;
			default:
				break;
		}
		
		setColor(_baseColor);
	}

	@Override
	public Actor hit(float x, float y, boolean touchable) {
		if (Intersector.isPointInPolygon(getPolygon(), new Vector2(x, y))) {
			return this;
		}
		else {
			return null;
		}
	}

	@Override
	protected void sizeChanged() {
		setOrigin(getWidth() / 2, getHeight() / 2);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		Color color = getColor().cpy();
		
		color.a *= parentAlpha;
		
		
		TextureRegion fieldTexture = Assets.Textures.BASE_FIELD;
		
		
		switch(_state){
			case ATTACKABLE:
				color.lerp(1f, 0, 0, 1, 1f);
				break;
			case MOVEABLE:
				color.lerp(0, 0, 0, 0, 0.5f);
				break;
			case NORMAL:
				break;
			case SPAWNABLE:
				color.lerp(0, 0, 0, 0, 0.5f);
				break;
			default:
				break;
		}
		
		
		batch.setColor(color);
		
		float rot = getRotation() + 90;
		float ox = getOriginX();
		float oy = getOriginY();
		float sx = getScaleX();
		batch.draw(fieldTexture, getX(), getY(), ox, oy, getWidth(), getHeight(), getScaleX(), getScaleY(), rot, true);
	}

	public float getRadius() {
		return getWidth() / 2;
	}

	public Vector2 getCenter() {
		float radius = getRadius();
		return new Vector2(getX() + radius, getY() + radius);
	}

	public Array<Vector2> getPolygon() {
		Array<Vector2> polygon = new Array<Vector2>(true, 6);

		Vector2 center = new Vector2(getRadius(), getRadius());
		Vector2 ray = new Vector2(0, getRadius());

		// rotate ray and add to center to get all points
		for (int i = 0; i < 6; ++i) {
			ray.rotate(60);
			polygon.add(center.cpy().add(ray));
		}

		return polygon;
	}

	public Field getField() {
		return _field;
	}

	public void setState(State state){
		_state = state;
	}
	
	public State getState(){
		return _state;
	}
	
	
	public Color getBaseColor(){
		return _baseColor;
	}
}
