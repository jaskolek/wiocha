package pl.wiocha.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class ShadowActor extends Actor {

	private TextureRegion _texture;

	public ShadowActor(TextureRegion texture) {
		super();
		_texture = texture;
	}

	@Override
	protected void sizeChanged() {
		setOrigin(getWidth() / 2, getHeight() / 2);
	}


	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(0, 0, 0, getColor().a);

		float rot = getRotation();
		float ox = getOriginX();
		float oy = getOriginY();
		batch.draw(_texture, getX(), getY(), ox, oy, getWidth(), getHeight(), getScaleX(), getScaleY(), rot, true);

		super.draw(batch, parentAlpha);
	}

	public void setAlpha(float alpha) {
		getColor().a = alpha;
	}
}
