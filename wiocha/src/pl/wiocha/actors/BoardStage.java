package pl.wiocha.actors;


import pl.wiocha.enums.Constants;
import pl.wiocha.input.KeyboardTestInputProcessor;
import pl.wiocha.listeners.BoardListener;
import pl.wiocha.utils.SpeedController;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class BoardStage extends Stage {
	private BoardActor _board;
	private OrthographicCamera _camera;
	private SpeedController _cameraSpeedController;
	private Array<BoardListener> _listeners = new Array<BoardListener>();
	
	
	public BoardStage(BoardActor board, float width, float height, boolean keepAspectRatio, SpriteBatch batch) {
		super(width, height, keepAspectRatio, batch);
		_board = board;
		_camera = (OrthographicCamera)getCamera();
		_cameraSpeedController = new SpeedController(Constants.BoardCamera.MaxSpeed, Constants.BoardCamera.Friction);
		
		addActor(_board);
	}
	
	public InputProcessor getInputProcessor() {
		return new InputMultiplexer(new GestureDetector(_gestureListener), this, new KeyboardTestInputProcessor(_camera, _cameraSpeedController));
	}
	
	public void addBoardListener(BoardListener listener) {
		_listeners.add(listener);
	}
	
	public void removeBoardListener(BoardListener listener) {
		_listeners.removeValue(listener, true);
	}
	
	@Override
	public void act(float deltaTime) {
		_cameraSpeedController.update(deltaTime);
		_camera.translate(_cameraSpeedController.positionDelta);
		
		super.act(deltaTime);
	}


	private final GestureListener _gestureListener = new GestureAdapter() {
		private final Vector2 _tmpV2 = new Vector2();
		private final Vector3 _tmpV3 = new Vector3();
		
		
		@Override
		public boolean touchDown(float x, float y, int pointer, int button) {

			_cameraSpeedController.acceleration.set(0, 0);
			_cameraSpeedController.velocity.set(0, 0);
			_cameraSpeedController.positionDelta.set(0, 0);
			return true;
		}

		@Override
		public boolean fling(float velocityX, float velocityY, int button) {
//			_cameraSpeedController.velocity.set(0, 0);
//			_cameraSpeedController.positionDelta.set(0, 0);
//			
//			_cameraSpeedController.acceleration.x = -velocityX/5;
//			_cameraSpeedController.acceleration.y = velocityY/5;
//			_cameraSpeedController.isFrictionXOn = velocityX == 0;
//			_cameraSpeedController.isFrictionYOn = velocityY != 0;
			
			return true;
		}

		@Override
		public boolean pan(float x, float y, float deltaX, float deltaY) {
			_camera.position.add(-deltaX * _camera.zoom, deltaY * _camera.zoom, 0);
			return true;
		}

		@Override
		public boolean panStop(float x, float y, int pointer, int button) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean tap(float x, float y, int count, int button) {
			_camera.unproject(_tmpV3.set(x, y, 0));
			Actor actor = BoardStage.this.hit(_tmpV3.x, _tmpV3.y, true);
			
			if (actor instanceof UnitActor) {
				for (BoardListener listener : _listeners) {
					listener.unitTapped((UnitActor)actor);
				}
			}
			else if (actor instanceof FieldActor) {
				for (BoardListener listener : _listeners) {
					listener.fieldTapped((FieldActor)actor);
				}
			}
			
			return actor != null;
		}
	};
}
