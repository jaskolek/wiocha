package pl.wiocha.actors;

import static aurelienribon.tweenengine.TweenEquations.easeInBack;
import static aurelienribon.tweenengine.TweenEquations.easeOutBack;
import pl.wiocha.utils.TimelineBuilder;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class ButtonsBarActor extends Group {
	private Button _toggleButton = null;
	private ButtonGroup _buttonsGroup = new ButtonGroup();

	public static final float TOGGLE_BUTTON_PADDING = 16;
	public static final float BUTTONS_PADDING = 8;
	public static final float CLOSE_ANIMATION_TIME = 1f;
	public static final float OPEN_ANIMATION_TIME = 1f;

	public ButtonsBarActor() {
		super();
		setMinCheckCount(0);
		setMaxCheckCount(1);
	}

	public void setMinCheckCount(int checkCount) {
		_buttonsGroup.setMinCheckCount(checkCount);
	}

	public void setMaxCheckCount(int checkCount) {
		_buttonsGroup.setMaxCheckCount(checkCount);
	}

	public void addButton(Button button) {
		addActorBefore(_toggleButton, button);
		_buttonsGroup.add(button);
	}

	public void removeButton(Button button) {
		removeActor(button);
		_buttonsGroup.remove(button);
	}

	public void setToggleButton(Button button) {
		if (_toggleButton != null) {
			removeActor(_toggleButton);
			_toggleButton.removeListener(_toggleButtonListener);
			_toggleButton = null;
		}
		_toggleButton = button;
		_toggleButton.addListener(_toggleButtonListener);
		addActor(button);
	}

	public Button getChecked() {
		return _buttonsGroup.getChecked();
	}

	public void toggle() {
		_toggleButton.toggle();
	}

	public void setDisabled(boolean disabled) {
		for (Button button : _buttonsGroup.getButtons()) {
			button.setDisabled(disabled);
		}
		_toggleButton.setDisabled(disabled);
	}

	public void uncheckAll() {
		_buttonsGroup.uncheckAll();
	}

	public boolean isOpened() {
		return _toggleButton.isChecked();
	}

	public boolean isDisabled() {
		return _toggleButton.isDisabled();
	}

	public void animateOpen() {
		TimelineBuilder timeline = TimelineBuilder.createSequence();

		timeline.beginParallel();
		float x = _toggleButton.getWidth() + TOGGLE_BUTTON_PADDING;
		for (Button button : _buttonsGroup.getButtons()) {
			//@formatter:off
			timeline.beginSequence().setActor(button)
				.moveToX(x, OPEN_ANIMATION_TIME, easeOutBack)
			.end();
			//@formatter:on
			x += button.getWidth() + BUTTONS_PADDING;
		}
		timeline.end();
		timeline.start();
	}

	public void animateClose() {
		TimelineBuilder timeline = TimelineBuilder.createSequence();

		timeline.beginParallel();
		for (Button button : _buttonsGroup.getButtons()) {
			//@formatter:off
			timeline.beginSequence().setActor(button)
				.moveToX(0, CLOSE_ANIMATION_TIME, easeInBack)
			.end();
			//@formatter:on
		}
		timeline.end();
		timeline.start();
	}

	public void open() {
		_toggleButton.setChecked(true);
	}

	public void close() {
		_toggleButton.setChecked(false);
	}

	private final ChangeListener _toggleButtonListener = new ChangeListener() {
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			if (isOpened()) {
				animateOpen();
			}
			else {
				animateClose();
				uncheckAll();
			}
		}
	};
}
