package pl.wiocha.actors;

import pl.wiocha.Assets;
import pl.wiocha.model.UnitParameters;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class SpawnUnitButton extends Button {

	private UnitParameters _unitParameters;

	public SpawnUnitButton(UnitParameters unitParameters) {
		super();
		Drawable image = new TextureRegionDrawable(new TextureRegion(Assets.Textures.get(unitParameters.spawnButtonImg)));
		Drawable imageChecked = new TextureRegionDrawable(new TextureRegion(Assets.Textures.get(unitParameters.spawnButtonImgChecked)));
		ButtonStyle style = new ButtonStyle(image, null, imageChecked);

		setStyle(style);
		setWidth(getPrefWidth());
		setHeight(getPrefHeight());
		_unitParameters = unitParameters;
	}

	public UnitParameters getUnitParameters() {
		return _unitParameters;
	}

	public void setUnitParameters(UnitParameters unitParameters) {
		_unitParameters = unitParameters;
	}


}
