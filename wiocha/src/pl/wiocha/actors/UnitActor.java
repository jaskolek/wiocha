package pl.wiocha.actors;

import pl.wiocha.Assets;
import pl.wiocha.model.Unit;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class UnitActor extends Actor {
	protected Unit _unit;
	protected TextureRegion _texture;

	private boolean _isDisabled = true;
	private boolean _isSelected = false;
	
	public UnitActor(Unit unit) {
		_unit = unit;
		_texture = new TextureRegion(Assets.Textures.get(unit.parameters.img));
	}

	@Override
	protected void sizeChanged() {
		setOrigin(getWidth() / 2, getHeight() / 2);
	}



	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor());

		float rot = getRotation();
		float ox = getOriginX();
		float oy = getOriginY();
		batch.draw(_texture, getX(), getY(), ox, oy, getWidth(), getHeight(), getScaleX(), getScaleY(), rot, true);

		super.draw(batch, parentAlpha);
	}

	public Unit getUnit() {
		return _unit;
	}
	
	public TextureRegion getTexture(){
		return _texture;
	}
	


	public boolean isDisabled() {
		return _isDisabled;
	}

	public void setDisabled(boolean isDisabled) {
		_isDisabled = isDisabled;
	}

	public boolean isSelected() {
		return _isSelected;
	}

	public void setSelected(boolean isSelected) {
		_isSelected = isSelected;
	}
}
