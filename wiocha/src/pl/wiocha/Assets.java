package pl.wiocha;

import pl.wiocha.utils.Content;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

	/**
	 * nie mam pojecia jak to inaczej zrobic zeby dobrze sie z tego korzystalo 
	 * - daje tutaj ale wydaje mi sie ze to bedzie do przerobienia
	 */
	private static AssetManager _manager;

	public static class Textures {
		public static Texture get(String path) {
			return _manager.get(path, Texture.class);
		}

		public static TextureRegion BASE_FIELD;
		public static TextureRegion WIZARD;

		public static TextureRegion HOUSE_1;
		public static TextureRegion HOUSE_2;
		public static TextureRegion HOUSE_3;
		public static TextureRegion WINDMILL;

		public static Texture ADD_UNITS_BUTTON;
		public static Texture ADD_UNITS_BUTTON_CHECKED;
	}

	public static class Sounds {

	}

	public static class Music {

	}
	
	public static Content Content;

	public static AssetManager getManager() {
		return _manager;
	}

	public static void init(AssetManager manager) {
		_manager = manager;
		Content = new Content(Gdx.files.internal("data/content.json2"));
	}



}
