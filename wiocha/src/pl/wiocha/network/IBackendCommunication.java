package pl.wiocha.network;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.network.listeners.*;

public interface IBackendCommunication {
	void initialize();
	
	/**
	 * Connects to the AppWarpServer
	 * 
	 * @param username	has to be unique username, e.g. e-mail or Facebook address
	 */
	void connect(String username, String password, IConnectionListener connectionListener);
	
	/**
	 * Finds opponent of opposite player type or creates room to wait for one if there is no available player.
	 * 
	 * @param thisPlayerType	given type can be None, then it will be randomized by server
	 * @param gameSessionListener
	 */
	void createOrJoinRoom(PlayerType thisPlayerType, final IStartGameSessionListener startGameSessionListener);

	String getPlayerId();
}
