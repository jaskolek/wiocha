package pl.wiocha.network.listeners;

public interface IConnectionListener {
	void onConnectionResult(boolean successfullyConnected);
}
