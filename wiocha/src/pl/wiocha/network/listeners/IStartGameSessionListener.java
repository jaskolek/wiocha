package pl.wiocha.network.listeners;

public interface IStartGameSessionListener {
	public static final int GENERAL_FAILURE = 0;
	//public static final int AWAITING_FOR_OPPONENT = 1;
	//public static final int OPPONENT_ASSIGNED = 3;//typeof(obj) = PlayerType
	public static final int GAME_STARTED = 4;//typeof(obj) = map data
	
	void onNewGameSessionStatus(int statusCode, Object obj);
}
