package pl.wiocha.network.appwarps2;

import com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener;

public abstract class RoomRequestAdapter implements RoomRequestListener {

	@Override
	public void onGetLiveRoomInfoDone(LiveRoomInfoEvent event) {
	}

	@Override
	public void onJoinRoomDone(RoomEvent event, String arg1) {
	}

	@Override
	public void onLeaveRoomDone(RoomEvent event) {
	}

	@Override
	public void onLockPropertiesDone(byte result) {
	}

	@Override
	public void onRPCDone(byte result, String function, Object returnValue) {
	}

	@Override
	public void onSetCustomRoomDataDone(LiveRoomInfoEvent event) {
	}

	@Override
	public void onSubscribeRoomDone(RoomEvent event) {
	}

	@Override
	public void onUnSubscribeRoomDone(RoomEvent event) {
	}

	@Override
	public void onUnlockPropertiesDone(byte result) {
	}

	@Override
	public void onUpdatePropertyDone(LiveRoomInfoEvent event, String arg1) {
	}

}
