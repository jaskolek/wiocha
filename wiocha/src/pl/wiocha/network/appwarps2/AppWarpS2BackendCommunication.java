package pl.wiocha.network.appwarps2;

import java.util.HashMap;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.enums.RpcConstants;
import pl.wiocha.network.IBackendCommunication;
import pl.wiocha.network.listeners.IConnectionListener;
import pl.wiocha.network.listeners.IStartGameSessionListener;
import pl.wiocha.network.messages.LoginResultMessage;
import pl.wiocha.network.messages.StartGameMessage;
import pl.wiocha.utils.Logger;
import pl.wiocha.utils.SerializationUtils;
import pl.wiocha.utils.callbacks.ResultCallback;
import pl.wiocha.utils.callbacks.StringResultCallback;

import com.shephertz.app42.gaming.multiplayer.client.WarpClient;
import com.shephertz.app42.gaming.multiplayer.client.command.WarpResponseResultCode;
import com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener;

public class AppWarpS2BackendCommunication implements IBackendCommunication {
	private final static String API_KEY = "89261bf8-4455-4fe4-8";// "c442360d7121c20e72257624c21c7a7e5c69d6e83050114b1ecfe3c2650e25b0";
	private final static String SECRET_KEY = "058163795b97928e1fcce8c11e99aa110896a80785c248b72f66d7ad60464d68";
	private final static String TEST_SERVER_HOST = "localhost";// = null;

	private WarpClient _client = null;
	private String _username;
	private boolean _isLoggedIn = false;
	private String _playerId;
	private RoomData _roomData;
	
	
	public void initialize() {
		WarpClient.initialize(API_KEY, TEST_SERVER_HOST != null ? TEST_SERVER_HOST : SECRET_KEY);
		try {
			_client = WarpClient.getInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void connect(String username, String password, final IConnectionListener connectionListener) {
		_username = username;
		
		// TODO this message will appear a little later than connectionListener.onConnectionResult will be called,
		//      so there is a possibility to make some kind of error, because playerId.
		//      Thing like "Counter" would be needed here.
		final NotifyAdapter loginDataListener = new NotifyAdapter() {
			@Override
			public void onUpdatePeersReceived(UpdateEvent event) {
				_client.removeNotificationListener(this);
				
				byte[] msgBytes = event.getUpdate();
				LoginResultMessage msg = SerializationUtils.<LoginResultMessage>deserialize(msgBytes);
				_playerId = msg.playerId;
			}
		};
		_client.addNotificationListener(loginDataListener);
		
		_client.addConnectionRequestListener(new ConnectionRequestAdapter() {
			@Override
			public void onConnectDone(ConnectEvent event, String description) {
				_client.removeConnectionRequestListener(this);
				
				_isLoggedIn = event.getResult() == WarpResponseResultCode.SUCCESS;
				connectionListener.onConnectionResult(_isLoggedIn);
				
				if (!_isLoggedIn) {
					_client.removeNotificationListener(loginDataListener);
				}
			}
		});
		
		_client.connectWithUserName(username, password);
	}
	
	public String getPlayerId() {
		return _playerId;
	}


	/**
	 * Creates and joins or joins to existing room.
	 * Player will be connected to play as chosen player type.
	 * 
	 * @param thisPlayerType	when type is None then it's being randomized or chosen due to enemy preference
	 */
	public void createOrJoinRoom(final PlayerType thisPlayerType, final IStartGameSessionListener startGameSessionListener) {
		// 1. Try to find already existing open room to join in.
		_client.addZoneRequestListener(new ZoneRequestAdapter() {
			public void onRPCDone(byte result, String function, Object returnValue) {
				if (!function.equals(RPC_FIND_ROOM_FOR_PLAYER)) {
					return;
				}
				_client.removeZoneRequestListener(this);
				
				if (result != WarpResponseResultCode.SUCCESS) {
					return;
				}
				
				final String roomId = (String) returnValue;

				if (roomId == null || roomId.equals("")) {
					Logger.debug("creating room...");
					
					// 2.a. there is no open room. Create our own 
					createRoom(thisPlayerType, new StringResultCallback() {
						public void onCallback(String roomId) {
							boolean wasRoomCreatedProperly = roomId != null;

							if (wasRoomCreatedProperly) {
								joinRoom(roomId, new ResultCallback<StartGameMessage>() {
									public void onCallback(StartGameMessage gameInfo) {
										int statusCode = gameInfo != null
											? IStartGameSessionListener.GAME_STARTED
											: IStartGameSessionListener.GENERAL_FAILURE;
										
										startGameSessionListener.onNewGameSessionStatus(statusCode, gameInfo);
									}
								});
							}
							else {
								startGameSessionListener.onNewGameSessionStatus(IStartGameSessionListener.GENERAL_FAILURE, null);
							}
						}
					});
				}
				else {
					Logger.debug("found room = " + roomId);
					
					// 2.b. Some open room was found, join in 
					joinRoom(roomId, new ResultCallback<StartGameMessage>() {
						public void onCallback(StartGameMessage gameInfo) {
							int statusCode = gameInfo != null
									? IStartGameSessionListener.GAME_STARTED
									: IStartGameSessionListener.GENERAL_FAILURE;

							startGameSessionListener.onNewGameSessionStatus(statusCode, gameInfo);
						}
					});
				}
			}
		});
		_client.invokeZoneRPC(RPC_FIND_ROOM_FOR_PLAYER, thisPlayerType.ordinal());
	}

	private void createRoom(final PlayerType thisPlayerType, final StringResultCallback callback) {
		String owner = _username;

		_client.addZoneRequestListener(new ZoneRequestAdapter() {
			public void onCreateRoomDone(RoomEvent event, String obj) {
				_client.removeZoneRequestListener(this);

				String roomId = event.getData().getId();
				callback.onCallback(roomId);
			}
		});
		HashMap<String, Object> roomParams = new HashMap<String, Object>();
		roomParams.put(RpcConstants.RoomParam_PreferredPlayerType, String.valueOf(thisPlayerType.toInt()));
		
		_client.createRoom("room name like any other", owner, 2, roomParams);
	}
 
	/**
	 * Joins to selected room. Player type of this player will be returned.
	 * 
	 * @param roomId
	 * @param callback
	 */
	private void joinRoom(final String roomId, final ResultCallback<StartGameMessage> callback) {
		_client.addRoomRequestListener(new RoomRequestAdapter() {
			public void onJoinRoomDone(RoomEvent event, String arg1) {
				_client.removeRoomRequestListener(this);
				
				_roomData = event.getData();
				
				_client.addRoomRequestListener(new RoomRequestAdapter() {
					public void onSubscribeRoomDone(RoomEvent event) {
						_client.removeRoomRequestListener(this);
						
						// TODO ?
						Logger.log("subscribed to room");
					}
				});
				
				// Server will send my player type
				_client.addNotificationListener(new NotifyAdapter() {
					public void onUpdatePeersReceived(UpdateEvent event) {
						_client.removeNotificationListener(this);
						
						byte[] bytes = event.getUpdate();
						StartGameMessage msg = SerializationUtils.<StartGameMessage>deserialize(bytes);
						
						callback.onCallback(msg);
					}
				});
				
				_client.subscribeRoom(_roomData.getId());
			}
		});
		_client.joinRoom(roomId);
	}

	private static final String RPC_FIND_ROOM_FOR_PLAYER = "findRoomForPlayer";
}
