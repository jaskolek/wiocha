package pl.wiocha.network.appwarps2;

import com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener;

public abstract class ZoneRequestAdapter implements ZoneRequestListener {

	@Override
	public void onCreateRoomDone(RoomEvent evt, String arg1) {
	}

	@Override
	public void onDeleteRoomDone(RoomEvent evt, String arg1) {
	}

	@Override
	public void onGetAllRoomsDone(AllRoomsEvent evt) {
	}

	@Override
	public void onGetLiveUserInfoDone(LiveUserInfoEvent evt) {
	}

	@Override
	public void onGetMatchedRoomsDone(MatchedRoomsEvent evt) {
	}

	@Override
	public void onGetOnlineUsersDone(AllUsersEvent evt) {
	}

	@Override
	public void onRPCDone(byte result, String function, Object returnValue) {
	}

	@Override
	public void onSetCustomUserDataDone(LiveUserInfoEvent evt) {
	}

}
