package pl.wiocha.network.appwarps2;

import com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener;

public class ConnectionRequestAdapter implements ConnectionRequestListener {
	@Override
	public void onConnectDone(ConnectEvent event, String description) {
	}

	@Override
	public void onDisconnectDone(ConnectEvent event) {
	}

	@Override
	public void onInitUDPDone(byte result) {
	}
}
