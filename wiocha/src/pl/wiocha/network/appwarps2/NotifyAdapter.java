package pl.wiocha.network.appwarps2;

import java.util.HashMap;

import com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.LobbyData;
import com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent;
import com.shephertz.app42.gaming.multiplayer.client.events.RoomData;
import com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent;
import com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener;

public abstract class NotifyAdapter implements NotifyListener {
	/**
	 * Invoked when a room is created. Lobby subscribers will receive this.
	 * 
	 * @param event
	 */
	public void onRoomCreated(RoomData event) {
	}

	/**
	 * Invoked when a room is deleted. Lobby subscribers will receive this.
	 * 
	 * @param event
	 */
	public void onRoomDestroyed(RoomData event) {
	}

	/**
	 * Invoked when a user leaves a room. Lobby and the concerned room subscribers
	 * will receive this.
	 * 
	 * @param event
	 * @param username
	 */
	public void onUserLeftRoom(RoomData event, String username) {
	}

	/**
	 * Invoked when a user joins a room. Lobby and the concerned room subscribers
	 * will receive this.
	 * 
	 * @param event
	 * @param username
	 */
	public void onUserJoinedRoom(RoomData event, String username) {
	}

	/**
	 * Invoked when a user leaves a lobby. Lobby subscribers will receive this.
	 * 
	 * @param event
	 * @param username
	 */
	public void onUserLeftLobby(LobbyData event, String username) {
	}

	/**
	 * Invoked when a user joins a lobby. Lobby subscribers will receive this.
	 * 
	 * @param event
	 * @param username
	 */
	public void onUserJoinedLobby(LobbyData event, String username) {
	}

	/**
	 * Invoked when a joined user sends a chat. Rooms subscribers will receive this.
	 * 
	 * @param event
	 */
	public void onChatReceived(ChatEvent event) {
	}

	/**
	 * Invoked when a private chat is received from the given sender.
	 * 
	 * @param sender
	 * @param message
	 */
	public void onPrivateChatReceived(String sender, String message) {
	}

	/**
	 * Invoked when a updatePeers request is sent in one of the subscribed rooms.
	 * 
	 * @param event
	 */
	public void onUpdatePeersReceived(UpdateEvent event) {
	}

	/**
	 * Invoked when a user changes the properties of a subscribed room property.
	 * 
	 * @param event
	 * @param username
	 * @param properties
	 * @param lockProperties
	 */
	public void onUserChangeRoomProperty(RoomData event, String username, HashMap<String, Object> properties, HashMap<String, String> lockProperties) {
	}

	/**
	 * Invoked when a user's move is completed in a turn based room
	 * 
	 * @param moveEvent
	 */
	public void onMoveCompleted(MoveEvent moveEvent) {
	}

	/*
	 * Invoked when a user's start game in a turn based room
	 */
	public void onGameStarted(String sender, String roomId, String nextTurn) {
	}

	/*
	 * Invoked when a user's stop game in a turn based room
	 */
	public void onGameStopped(String sender, String roomId) {
	}

	/**
	 * Invoked to indicate that a user has lost connectivity. Subscribers of the users location
	 * will receive this.
	 * 
	 * @param locid
	 * @param isLobby
	 * @param username
	 */
	public void onUserPaused(String locid, boolean isLobby, String username) {
	}

	/**
	 * Invoked when a user's connectivity is restored. Subscribers of the users location
	 * will receive this.
	 * 
	 * @param locid
	 * @param isLobby
	 * @param username
	 */
	public void onUserResumed(String locid, boolean isLobby, String username) {
	}
}
