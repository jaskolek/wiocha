package pl.wiocha.logic;

import pl.wiocha.model.Unit;

public class CharacterMoveSimulationResult {
	public Unit characterUnit;
	public MapPath takenPath;//contains starting position
	public boolean wasUnitMoved;
	
	
	public CharacterMoveSimulationResult(Unit characterUnit) {
		this.characterUnit = characterUnit;
	}
	
	public int getStepsCount() {
		return takenPath.getLength() - 1;
	}
	
	public void setPath(MapPath takenPath) {
		this.takenPath = takenPath;
		wasUnitMoved = getStepsCount() > 0;
	}
}
