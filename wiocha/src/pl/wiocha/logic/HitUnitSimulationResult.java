package pl.wiocha.logic;

public class HitUnitSimulationResult {

	public int hitPointsTaken;

	public HitUnitSimulationResult(int hitPointsTaken) {
		super();
		this.hitPointsTaken = hitPointsTaken;
	}
}
