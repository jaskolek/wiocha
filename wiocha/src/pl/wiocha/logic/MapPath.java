package pl.wiocha.logic;

import pl.wiocha.utils.AxialPosition;

import com.badlogic.gdx.utils.Array;

public class MapPath {
	public Array<AxialPosition> fieldList;
	
	public MapPath(Array<AxialPosition> fieldList) {
		this.fieldList = fieldList;
	}

	public int getLength() {
		return fieldList.size;
	}

	public int getStepsCount() {
		return getLength() - 1;
	}
	
	public AxialPosition getStartingPosition() {
		return fieldList.first();
	}
	
	public AxialPosition getFinalPosition() {
		return fieldList.get(fieldList.size - 1);
	}
	
	public AxialPosition getNthPosition(int index) {
		return fieldList.get(index);
	}
}
