package pl.wiocha.logic;

import pl.wiocha.enums.PlayerType;

public class PlayerState {
	private PlayerType _playerType;
	private int _mana;
	private int _gold;
	private int _actionPoints;
	

	public PlayerState(PlayerType playerType) {
		_playerType = playerType;
	}
	
	public PlayerType getPlayerType() {
		return _playerType;
	}
	
	public int getMana() {
		return _mana;
	}

	public void setMana(int mana) {
		_mana = mana;
	}

	public int getGold() {
		return _gold;
	}

	public void setGold(int gold) {
		_gold = gold;
	}

	public int getActionPoints() {
		return _actionPoints;
	}

	public void setActionPoints(int actionPoints) {
		_actionPoints = actionPoints;
	}

	public void takeActionPoints(int takenActionPoints) {
		assert(_actionPoints >= takenActionPoints);
		
		_actionPoints -= takenActionPoints;
	}
}
