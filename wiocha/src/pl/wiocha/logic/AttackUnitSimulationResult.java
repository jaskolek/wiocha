package pl.wiocha.logic;

import com.badlogic.gdx.utils.Array;

import pl.wiocha.model.Unit;

public class AttackUnitSimulationResult {
	public Unit attacker;
	public Unit defender;
	public int actionPointsCost;
	public Array<HitUnitSimulationResult> hits = new Array<HitUnitSimulationResult>();
	
	public AttackUnitSimulationResult(Unit attacker, Unit defender) {
		super();
		this.attacker = attacker;
		this.defender = defender;
	}
}
