package pl.wiocha.logic;

import pl.wiocha.model.Unit;

public class SpawnUnitSimulationResult {
	public Unit spawnedUnit;
	public boolean wasUnitSpawned;
	public int actionPointsCost;
	
	
	public SpawnUnitSimulationResult(Unit unit) {
		this.spawnedUnit = unit;
	}
}
