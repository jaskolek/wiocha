package pl.wiocha.logic.path_finders;

import pl.wiocha.model.Field;

/**
 * Predicate to define fields which are un/walkable.
 * 
 * @author nmk
 */
public interface IPathFinderFieldAcceptPredicate {
	boolean checkField(Field field);
}
