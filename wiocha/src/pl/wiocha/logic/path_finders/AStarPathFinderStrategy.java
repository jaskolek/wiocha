package pl.wiocha.logic.path_finders;

import java.util.HashMap;

import pl.wiocha.model.Field;
import pl.wiocha.model.Map;
import pl.wiocha.utils.AxialPosition;

import com.badlogic.gdx.utils.Array;

public class AStarPathFinderStrategy implements IPathFinderStrategy {

	public AStarPathFinderStrategy() {
		super();
	}

	/**
	 * Finds shortest path form start to end.
	 * Start and End are included in returned collection.
	 * If path is not found then returns empty collection.
	 */
	@Override
	public Array<AxialPosition> findPath(Map map, AxialPosition start, AxialPosition end, IPathFinderFieldAcceptPredicate fieldPredicate) {
		HashMap<AxialPosition, Float> gScore = new HashMap<AxialPosition, Float>();
		HashMap<AxialPosition, Float> fScore = new HashMap<AxialPosition, Float>();
		HashMap<AxialPosition, Float> hScore = new HashMap<AxialPosition, Float>();
		HashMap<AxialPosition, AxialPosition> cameFrom = new HashMap<AxialPosition, AxialPosition>();

		Array<AxialPosition> closedSet = new Array<AxialPosition>(false, 16);
		Array<AxialPosition> openSet = new Array<AxialPosition>(false, 16);

		openSet.add(start);

		gScore.put(start, 0f);
		fScore.put(start, 0f);
		hScore.put(start, start.getDistance(end));


		boolean pathFound = false;
		while (openSet.size > 0 && pathFound == false) {
			AxialPosition x = null;
			float minF = Float.MAX_VALUE;
			// Getting best position from notVisited
			for (int i = 0; i < openSet.size; ++i) {
				AxialPosition position = openSet.get(i);
				float positionFScore = fScore.get(position);
				if (minF > positionFScore) {
					x = position;
					minF = positionFScore;
				}
			}

			if (x.equals(end)) {
				pathFound = true;
			}
			else {
				openSet.removeValue(x, false);
				closedSet.add(x);

				// wywalic pola na ktore nie mozemy stanac
				Array<AxialPosition> neighbors = x.getNeighbors();
				for (int i = 0; i < neighbors.size; ++i) {
					AxialPosition y = neighbors.get(i);

					if (!closedSet.contains(y, false)
							&& map.isPositionInRange(y)) {
						Field field = map.getField(y);
						
						if (fieldPredicate.checkField(field) && field.getUnits().size == 0) {
							// new, not visited field
							float tentativeGScore = gScore.get(x) + 1;
							boolean tentativeIsBetter = false;

							if (!openSet.contains(y, false)) {
								openSet.add(y);
								hScore.put(y, y.getDistance(end));
								tentativeIsBetter = true;
							}
							else if (tentativeGScore < gScore.get(y)) {
								tentativeIsBetter = true;
							}
							if (tentativeIsBetter) {
								cameFrom.put(y, x);
								gScore.put(y, tentativeGScore);
								fScore.put(y, gScore.get(y) + hScore.get(y));
							}
						}
					}
				}
			}
		}
		Array<AxialPosition> path = new Array<AxialPosition>();
		if (pathFound == true) {
			AxialPosition currentPosition = end;
			path.add(end);
			while (!currentPosition.equals(start)) {
				currentPosition = cameFrom.get(currentPosition);
				path.add(currentPosition);
			}
		}
		path.reverse();
		return path;
	}

}
