package pl.wiocha.logic.path_finders;

import pl.wiocha.model.Map;
import pl.wiocha.utils.AxialPosition;

import com.badlogic.gdx.utils.Array;

public interface IPathFinderStrategy {
	public Array<AxialPosition> findPath(Map map, AxialPosition start, AxialPosition end, IPathFinderFieldAcceptPredicate fieldPredicate);
}
