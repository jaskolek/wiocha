package pl.wiocha.logic.path_finders;

import pl.wiocha.model.Field;

public class DummyPathFinderFieldAcceptPredicate implements IPathFinderFieldAcceptPredicate {

	@Override
	public boolean checkField(Field field) {
		return true;
	}

}
