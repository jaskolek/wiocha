package pl.wiocha;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.model.Unit;
import pl.wiocha.model.Field;
import pl.wiocha.model.Field.Type;
import pl.wiocha.model.Map;
import pl.wiocha.utils.AxialPosition;

import com.badlogic.gdx.utils.Array;

public class MapFactory {

	private UnitParametersManager _unitParametersManager;

	public MapFactory(UnitParametersManager unitParametersManager) {
		super();
		_unitParametersManager = unitParametersManager;
	}

	public Map createSquareMap(int sideLength) {
		return createRectangleMap(sideLength, sideLength);
	}

	public Map createRectangleMap(int rows, int cols) {
		int arrayCols = cols + rows / 2;
		Map map = new Map(rows, arrayCols);

		for (int row = 0; row < rows; ++row) {
			int start = rows / 2 - row / 2;
			int end = start + cols;
			for (int col = start; col < end; ++col) {
				map.setField(row, col, new Field(row, col));
			}
		}

		return map;
	}

	public Map createTriangleMap(int sideLength) {
		Map map = new Map(sideLength, sideLength);
		for (int row = 0; row < sideLength; ++row) {
			for (int col = 0; col < sideLength - row; ++col) {
				map.setField(row, col, new Field(row, col));
			}
		}
		return map;
	}

	public Map createRhombusMap(int rows, int cols) {
		Map map = new Map(rows, cols);
		for (int row = 0; row < rows; ++row) {
			for (int col = 0; col < cols; ++col) {
				map.setField(row, col, new Field(row, col));
			}
		}
		return map;
	}

	public Map createHexagonMap(int sideLength) {
		Map map = new Map(sideLength * 2 - 1, sideLength * 2 - 1);

		for (int row = 0; row < 2 * sideLength - 1; ++row) {
			int start = Math.max(0, sideLength - 1 - row);
			int end = Math.min(sideLength * 2 - 1, sideLength * 3 - row - 2);
			for (int col = start; col < end; ++col) {
				Field field = new Field(row, col);
				if( col == start || col == end-1 || row == 0 || row == 2 * sideLength - 2 ){
					field.isAttackerSpawnAllowed = true;
				}
				
				map.setField(row, col, field);
			}
		}

		return map;
	}

	public Map createMap(String mapName) {
		// TODO read map from file
		return createTestMap();
	}

	public Map createTestMap() {
		Map map = createHexagonMap(6);

		Unit wizard = new Unit(_unitParametersManager.get("wizard"), PlayerType.Defense);
		map.addUnit(wizard);
		map.setUnit(wizard, new AxialPosition(2, 5));

		Unit house = new Unit(_unitParametersManager.get("house_3"), PlayerType.Defense);
		map.addUnit(house);
		map.setUnit(house, new AxialPosition(3, 5));


		Unit windmill = new Unit(_unitParametersManager.get("windmill"), PlayerType.Defense);
		map.addUnit(windmill);
		map.setUnit(windmill, new AxialPosition(6, 6));

		Array<AxialPosition> waterPositions = new Array<AxialPosition>();

		waterPositions.add(new AxialPosition(6, 5));
		waterPositions.add(new AxialPosition(5, 4));
		waterPositions.add(new AxialPosition(5, 5));
		waterPositions.add(new AxialPosition(4, 2));
		waterPositions.add(new AxialPosition(5, 3));
		waterPositions.add(new AxialPosition(6, 6));
		waterPositions.add(new AxialPosition(7, 6));
		waterPositions.add(new AxialPosition(6, 6));
		waterPositions.add(new AxialPosition(4, 3));


		Array<AxialPosition> groundPositions = new Array<AxialPosition>();

		groundPositions.add(new AxialPosition(7, 5));
		groundPositions.add(new AxialPosition(7, 6));
		groundPositions.add(new AxialPosition(6, 6));
		groundPositions.add(new AxialPosition(5, 6));
		groundPositions.add(new AxialPosition(6, 7));
		groundPositions.add(new AxialPosition(7, 7));
		groundPositions.add(new AxialPosition(8, 6));
		groundPositions.add(new AxialPosition(8, 5));
		groundPositions.add(new AxialPosition(7, 4));
		groundPositions.add(new AxialPosition(8, 7));
		groundPositions.add(new AxialPosition(9, 6));

		for (AxialPosition waterPosition : waterPositions) {
			map.getField(waterPosition).type = Type.WATER;
		}
		for (AxialPosition groundPosition : groundPositions) {
			map.getField(groundPosition).type = Type.GROUND;
		}

		return map;
	}
}
