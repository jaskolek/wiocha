package pl.wiocha;

import java.util.HashMap;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.logic.PlayerState;
import pl.wiocha.model.Map;
import pl.wiocha.network.IBackendCommunication;
import pl.wiocha.network.appwarps2.AppWarpS2BackendCommunication;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class DataContext {
	// Managers
	public SpriteBatch batch;
	public Skin skin;
	public UnitParametersManager unitParametersManager;
	public MapFactory mapFactory;
	public IBackendCommunication backend;

	// Actual data
	public Map map;
	public PlayerType thisPlayerId, currentPlayerId;
	public HashMap<PlayerType, PlayerState> playerStates = new HashMap<PlayerType, PlayerState>();
	
	
	public DataContext() {
		skin = new Skin(Gdx.files.internal("data/skin/uiskin.json"));
		unitParametersManager = new UnitParametersManager();
		mapFactory = new MapFactory(unitParametersManager);
		backend = new AppWarpS2BackendCommunication();
	}
	
	public PlayerState getThisPlayerState() {
		return playerStates.get(thisPlayerId);
	}
	
	public PlayerState getCurrentPlayerState() {
		return playerStates.get(currentPlayerId);
	}
}
