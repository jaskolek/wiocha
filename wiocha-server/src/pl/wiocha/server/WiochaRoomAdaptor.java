package pl.wiocha.server;

import java.util.ArrayList;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.network.messages.StartGameMessage;
import pl.wiocha.server.model.PlayerInfo;
import pl.wiocha.utils.SerializationUtils;

import com.shephertz.app42.server.idomain.BaseRoomAdaptor;
import com.shephertz.app42.server.idomain.HandlingResult;
import com.shephertz.app42.server.idomain.IRoom;
import com.shephertz.app42.server.idomain.IUser;

public class WiochaRoomAdaptor extends BaseRoomAdaptor {
	private ArrayList<PlayerInfo> _players = new ArrayList<>();
	private volatile int _readyPlayersCount = 0;
	
	
	WiochaRoomAdaptor() {
	}

	@Override
	public void onUserLeaveRequest(IUser user) {
		System.out.println(user.getName() + " left room " + user.getLocation().getId());
		PlayerInfo player = PlayerList.getInstance().getPlayer(user);
		_players.remove(player);
	}

	@Override
	public void handleUserJoinRequest(IUser user, HandlingResult result) {
		PlayerInfo player = PlayerList.getInstance().getPlayer(user);
		
		if (_players.size() > 0) {
			PlayerInfo firstPlayer = _players.get(0);
			if (firstPlayer.type == PlayerType.None) {
				if (player.type == PlayerType.None) {
					player.type = PlayerType.getRandom();
				}
				
				firstPlayer.type = player.type.getOpposite();
			}
			
			if (player.type == PlayerType.None) {
				player.type = firstPlayer.type.getOpposite();
			}
		}
		
		_players.add(player);
		
		System.out.println(user.getName() + " joined room as playerType=" + player.type);
	}

	@Override
	public synchronized void handleUserSubscribeRequest(IUser sender, HandlingResult result) {
		_readyPlayersCount += 1;
		
		if (_readyPlayersCount == 2) {
			int attackerIndex =  _players.get(0).type == PlayerType.Attack ? 0 : 1;
			int defenderIndex = attackerIndex == 0 ? 1 : 0;
			PlayerInfo attacker = _players.get(attackerIndex);
			PlayerInfo defender = _players.get(defenderIndex);
			
			StartGameMessage msg = new StartGameMessage();
			msg.attackerPlayerId = attacker.uniqueId;
			msg.defenderPlayerId = defender.uniqueId;
			msg.mapId = "testmap";
			
			byte[] msgBytes = SerializationUtils.serialize(msg);
			
			for (PlayerInfo player : _players) {
				player.user.SendUpdatePeersNotification(msgBytes, false);
			}
			
			System.out.println("Game started.");
		}
	}

	@Override
	public void handleChatRequest(IUser sender, String message, HandlingResult result) {
		System.out.println(sender.getName() + " says " + message);

		IRoom room = sender.getLocation();
		sender.SendChatNotification("Admin", "You are not allowed to use abusive language", room);
		result.code = 1;
		result.description = "Bad Words Used";
		result.sendResponse = true;
		result.sendNotification = false;
	}

	@Override
	public void onTimerTick(long time) {
		// TODO Auto-generated method stub
		super.onTimerTick(time);
	}
	
	
}
