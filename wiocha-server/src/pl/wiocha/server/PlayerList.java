package pl.wiocha.server;

import java.util.HashMap;
import java.util.HashSet;

import pl.wiocha.server.model.PlayerInfo;

import com.shephertz.app42.server.idomain.IUser;

public class PlayerList {
	private static PlayerList _instance = null;
	
	// TODO znalezc bardziej bezpieczny singleton dla javy
	public static PlayerList getInstance() {
		if (_instance == null) {
			_instance = new PlayerList();
		}
	
		return _instance;
	}
	
	private HashMap<String, PlayerInfo> _players = new HashMap<>();
	
	public PlayerInfo addPlayer(IUser user) {
		String uniqueId = String.valueOf(user.hashCode());
		PlayerInfo player = new PlayerInfo(uniqueId, user);
		user.setCustomData(uniqueId);
		
		if (_players.containsKey(player.uniqueId)) {
			throw new RuntimeException("User with such unique id already exists!");
		}
		
		_players.put(player.uniqueId, player);
		
		return player;
	}
	
	public PlayerInfo getPlayerById(String uniqueId) {
		return _players.get(uniqueId);
	}
	
	public PlayerInfo getPlayer(IUser user) {
		return getPlayerById(getUserId(user));
	}
	
	public void removePlayer(String uniqueId) {
		_players.remove(uniqueId);
	}
	
	public void removePlayer(IUser user) {
		String uniqueId = getUserId(user);
		removePlayer(uniqueId);
	}
	
	public static String getUserId(IUser user) {
		return user.getCustomData();
	}
}
