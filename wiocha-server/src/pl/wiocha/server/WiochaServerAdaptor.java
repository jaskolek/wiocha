package pl.wiocha.server;

import com.shephertz.app42.server.idomain.BaseServerAdaptor;
import com.shephertz.app42.server.idomain.IZone;

public class WiochaServerAdaptor extends BaseServerAdaptor {
	@Override
	public void onZoneCreated(IZone zone) {
		System.out.println("Zone Created " + zone.getName() + " with key " + zone.getAppKey());
		zone.setAdaptor(new WiochaZoneAdaptor(zone));
	}
}
