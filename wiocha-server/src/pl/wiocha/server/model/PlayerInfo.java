package pl.wiocha.server.model;

import com.shephertz.app42.server.idomain.IUser;

import pl.wiocha.enums.PlayerType;

public class PlayerInfo {
	public final String uniqueId;
	public final IUser user;
	public PlayerType type = PlayerType.None;
	
	public PlayerInfo(String uniqueId, IUser user) {
		this.uniqueId = uniqueId;
		this.user = user;
	}
}
