package pl.wiocha.server.logic;

import java.util.ArrayList;
import java.util.List;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.server.model.PlayerInfo;

public class PlayerQueue {
	private List<PlayerInfo> awaitingPlayers = new ArrayList<PlayerInfo>();
	
	/**
	 * 
	 * @param currentPlayerType
	 * @return	null if there is no appropiate enemy
	 */
	synchronized public PlayerInfo popEnemy(PlayerType currentPlayerType) {
		if (currentPlayerType == PlayerType.None) {
			PlayerInfo player = awaitingPlayers.get(0);
			awaitingPlayers.remove(0);
			
			return player;
		}
		
		for (int i = 0, n = awaitingPlayers.size(); i < n; ++i) {
			PlayerInfo player = awaitingPlayers.get(i);
			
			if (player.type.isOpposite(currentPlayerType)) {
				awaitingPlayers.remove(i);
				return player;
			}
		}
		
		return null;
	}
	
	synchronized public void pushPlayer(PlayerInfo player) {
		// TODO
	}
	
	synchronized public void removePlayer(PlayerInfo player) {
		
	}
}
