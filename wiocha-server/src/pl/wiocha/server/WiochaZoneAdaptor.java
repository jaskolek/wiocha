package pl.wiocha.server;

import java.util.List;

import pl.wiocha.enums.PlayerType;
import pl.wiocha.enums.RpcConstants;
import pl.wiocha.network.messages.LoginResultMessage;
import pl.wiocha.server.model.PlayerInfo;
import pl.wiocha.utils.SerializationUtils;

import com.shephertz.app42.server.idomain.BaseZoneAdaptor;
import com.shephertz.app42.server.idomain.HandlingResult;
import com.shephertz.app42.server.idomain.IRoom;
import com.shephertz.app42.server.idomain.IUser;
import com.shephertz.app42.server.idomain.IZone;

public class WiochaZoneAdaptor extends BaseZoneAdaptor {
	private IZone _zone;
	
	
	WiochaZoneAdaptor(IZone zone) {
		_zone = zone;
	}

	@Override
	public void handleAddUserRequest(IUser user, String authData, HandlingResult result) {
		PlayerInfo player = PlayerList.getInstance().addPlayer(user);
		LoginResultMessage msg = new LoginResultMessage();
		msg.playerId = player.uniqueId;
		byte[] msgBytes = SerializationUtils.serialize(msg);

		user.SendUpdatePeersNotification(msgBytes, false);
	}

	@Override
	public void onUserRemoved(IUser user) {
		PlayerList.getInstance().removePlayer(user);
	}

	@Override
	public void handleCreateRoomRequest(IUser user, IRoom room, HandlingResult result) {
		System.out.println("Room Create Request " + room.getName() + " with ID " + room.getId());
		
		PlayerInfo player = PlayerList.getInstance().getPlayer(user);
		
		if (room.getProperties().containsKey(RpcConstants.RoomParam_PreferredPlayerType)) {
			String playerTypeIntAsStr = (String)room.getProperties().getOrDefault(RpcConstants.RoomParam_PreferredPlayerType, String.valueOf(PlayerType.None.toInt()));
			int playerTypeInt = Integer.parseInt(playerTypeIntAsStr);
			player.type = PlayerType.fromInt(playerTypeInt); 
		}
		
		room.setAdaptor(new WiochaRoomAdaptor());
	}

	@Override
	public void onAdminRoomAdded(IRoom room) {
		System.out.println("Room Created " + room.getName() + " with ID " + room.getId());
		room.setAdaptor(new WiochaRoomAdaptor());
	}

	/**
	 * RPC method. Looks for empty room and returns it's id or null if none is found.
	 */
	public String findRoomForPlayer(int requestedPlayerTypeAsInt) {
		final PlayerType requestedPlayerType = PlayerType.fromInt(requestedPlayerTypeAsInt);
		String foundRoomId = null;
		
		for (IRoom room : _zone.getRooms()) {
			List<IUser> roomUsers = room.getJoinedUsers();

			if (roomUsers.size() == 1) {
				IUser opponentUser = roomUsers.get(0);
				PlayerInfo opponentPlayer = PlayerList.getInstance().getPlayer(opponentUser);

				if (opponentPlayer.type == PlayerType.None || opponentPlayer.type != requestedPlayerType) {
					foundRoomId = room.getId();
				}
			}
		}

		return foundRoomId;
	}
}